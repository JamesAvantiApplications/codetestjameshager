//
//  ContactVCDoBCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC Date-of-Birth Cell
//

import UIKit

class ContactVCDoBCell: ContactVC2LineCell {
    
    var contact: Contact!
    
    var initialDoB: Date? {
        didSet {
            iniHasValue = initialDoB != nil
        }
    }
    
    var iniHasValue = false
    
    override var hasChanges: Bool {
        let curHasValue = contact.dateOfBirth != nil
        
        if curHasValue && iniHasValue {
            return contact.dateOfBirth! != initialDoB!
        } else if curHasValue != iniHasValue {
            return true
        } else {
            return false
        }
    }
    
    override var idForChange: NSObject {
        return dataType.rawValue as NSString
    }
    
    var datePicker: UIDatePicker!
    
    // MARK: - Configure
    
    func configure(contact: Contact, dispMode: ContactVCDispMode, delegate: ContactVCCellDelegate) {
        dataType = .dateOfBirth
        
        self.contact = contact
        configure(delegate: delegate, typeLabelChangesWithDispMode: false)
        
        initialDoB = contact.dateOfBirth
        
        if typeLabel == nil {
            setUpView()
        }
        
        setView(dispMode: dispMode)
        
        typeLabel.text = "birthday"
        dataTextField.text = contact.dateOfBirthString
        dataTextField.placeholder = "Birthday"
    }
    
    // MARK: - Setup View
    
    func setUpDatePicker() {
        Func.ifDebug(print("=== ContactVCDoBCell.\(#function) ==="))
        
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
        if let initialDoB = initialDoB {
            datePicker.date = initialDoB
        }
        
        dataTextField.inputView = datePicker
    }
    
    // MARK: - Actions
    
    override func handleTextFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== ContactVCDoBCell.\(#function) ==="))
        
        if textField.text! == "" {
            contact.dateOfBirth = nil
        }
        
        checkHasChangesHasChanged()
    }
    
    @objc func datePickerValueChanged(_ datePicker: UIDatePicker){
        Func.ifDebug(print("=== ContactVCDoBCell.\(#function) ==="))
        
        dataTextField.text = contact.dateString(forDate: datePicker.date)
        contact.dateOfBirth = datePicker.date
        
        checkHasChangesHasChanged()
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCDoBCell {
    
    override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        if textField.tag == 0 {
            return false
        }
        
        if datePicker == nil {
            setUpDatePicker()
        }
        
        return true
    }
}
