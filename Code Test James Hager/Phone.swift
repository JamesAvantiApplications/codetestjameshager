//
//  Phone.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(Phone)
class Phone: ContactData {
    
    var number: String {
        get { return numberSave }
        set {
            numberSave = newValue
            contact?.setSearchText()
        }
    }
    
    // MARK: - Other Variables
    
    var hasData: Bool {
        return number.count > 0
    }
    
    var numberToCall: String {
        return number.replacingOccurrences(of: " ", with: "")
    }
    
    // MARK: - Create/Delete Methods
    
    convenience init(label: PhoneLabel?, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.phone.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        self.number = ""
        self.label = label
        self.sort = -1
    }
    
    convenience init(number: String, label: PhoneLabel?, context: NSManagedObjectContext) {
        self.init(label: label, context: context)
        self.number = number
    }
    
    // MARK: - Describe
    
    func describe() {
        print("=== Phone.\(#function) === \(Func.debugDate())")
        print("--- -  number: '\(number)'")
        print("--- - .. sort: \(sort)")
        
        if let contact = contact {
            print("--- > contact: '\(contact.nameNatural)'")
        }
        
        label?.describe()
    }
}
