
import UIKit

class SettingsBulletItemCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, withDisclosureIndicator: Bool = false) {
        label.text = text
        if withDisclosureIndicator {
            accessoryType = .disclosureIndicator
        } else {
            selectionStyle = .none
        }
    }
}
