//
//  UIView+constraints.swift
//
//  Created by James Hager on 8/25/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  UIView extensions to pin a view to another view (called on the subview)
//
//  pinTypes and margins are arrays of [top, right, bottom, left]
//

import UIKit

extension UIView {
    
    func pinBottom(toTopOf pinView: UIView, margin: CGFloat? = nil) {
        if let margin = margin {
            self.bottomAnchor.constraint(equalTo: pinView.topAnchor, constant: -margin).isActive = true
        } else {
            self.bottomAnchor.constraint(equalTo: pinView.topAnchor).isActive = true
        }
    }
    
    func pinTop(toBottomOf pinView: UIView, margin: CGFloat? = nil) {
        if let margin = margin {
            self.topAnchor.constraint(equalTo: pinView.bottomAnchor, constant: margin).isActive = true
        } else {
            self.topAnchor.constraint(equalTo: pinView.bottomAnchor).isActive = true
        }
    }
    
    func pinLeft(toRightOf pinView: UIView, margin: CGFloat? = nil) {
        if let margin = margin {
            self.leftAnchor.constraint(equalTo: pinView.rightAnchor, constant: margin).isActive = true
        } else {
            self.leftAnchor.constraint(equalTo: pinView.rightAnchor).isActive = true
        }
    }
    
    func pinRight(toLeftOf pinView: UIView, margin: CGFloat? = nil) {
        if let margin = margin {
            self.rightAnchor.constraint(equalTo: pinView.leftAnchor, constant: -margin).isActive = true
        } else {
            self.rightAnchor.constraint(equalTo: pinView.leftAnchor).isActive = true
        }
    }
    
    // MARK: - Pin sides equal-to
    
    func pin(toView pinView: UIView, pinType pinTypeIn: AAPinType? = nil, margin: CGFloat? = nil) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinType defaults to .superview
        // margin defaults to 0
        
        var pinType: AAPinType!
        
        if pinTypeIn == nil {
            pinType = .superview
        } else {
            pinType = pinTypeIn
        }
        
        self.pinTopAnchor(toView: pinView, pinType: pinType, margin: margin)
        self.pinRightAnchor(toView: pinView, pinType: pinType, margin: margin)
        self.pinBottomAnchor(toView: pinView, pinType: pinType, margin: margin)
        self.pinLeftAnchor(toView: pinView, pinType: pinType, margin: margin)
    }
    
    func pin(toView pinView: UIView, pinTypes: [AAPinType?], margin: CGFloat? = nil) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinTypes is an array of [top, right, bottom, left]. If one side of the pinType is nil, that side isn't set.
        //
        // margin defaults to 0
        
        let margins: [CGFloat?] = [margin, margin, margin, margin]
        
        pin(toView: pinView, pinTypes: pinTypes, margins: margins)
    }
    
    func pin(toView pinView: UIView, pinTypes: [AAPinType?], margins: [CGFloat?]) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))Func.ifDebug(
        // pin "all" sides to corresponding sides of pinView
        //
        // pinTypes and margins are arrays of [top, right, bottom, left].
        //
        // If one side of the pinType is nil, that side isn't set.
        //
        // margin defaults to 0
        
        if let pinType = pinTypes[0] {
            self.pinTopAnchor(toView: pinView, pinType: pinType, margin: margins[0])
        }
        
        if let pinType = pinTypes[1] {
            self.pinRightAnchor(toView: pinView, pinType: pinType, margin: margins[1])
        }
        
        if let pinType = pinTypes[2] {
            self.pinBottomAnchor(toView: pinView, pinType: pinType, margin: margins[2])
        }
        
        if let pinType = pinTypes[3] {
            self.pinLeftAnchor(toView: pinView, pinType: pinType, margin: margins[3])
        }
    }
    
    // MARK: - Pin sides greater-than-or-equal-to
    
    func pinGreaterThanOrEqual(toView pinView: UIView, pinType pinTypeIn: AAPinType? = nil, margin marginIn: CGFloat? = nil) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinType defaults to .superview
        // margin defaults to 0
        
        var pinType: AAPinType!
        
        if pinTypeIn == nil {
            pinType = .superview
        } else {
            pinType = pinTypeIn
        }
        
        let margin = marginFrom(marginIn)
        
        self.pinTopAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        self.pinRightAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        self.pinBottomAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        self.pinLeftAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
    }
    
    func pinGreaterThanOrEqual(toView pinView: UIView, pinTypes: [AAPinType?], margin marginIn: CGFloat? = nil) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinTypes is an array of [top, right, bottom, left]. If one side of the pinType is nil, that side isn't set.
        //
        // margin defaults to 0
        
        let margin = marginFrom(marginIn)
        let margins: [CGFloat?] = [margin, margin, margin, margin]
        
        pinGreaterThanOrEqual(toView: pinView, pinTypes: pinTypes, margins: margins)
    }
    
    func pinGreaterThanOrEqual(toView pinView: UIView, pinTypes: [AAPinType?], margins: [CGFloat?]) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinTypes and margins are arrays of [top, right, bottom, left].
        //
        // If one side of the pinType is nil, that side isn't set.
        //
        // margin defaults to 0
        
        var margin: CGFloat!
        
        if let pinType = pinTypes[0] {
            margin = marginFrom(margins[0])
            self.pinTopAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        }
        
        if let pinType = pinTypes[1] {
            margin = marginFrom(margins[1])
            self.pinRightAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        }
        
        if let pinType = pinTypes[2] {
            margin = marginFrom(margins[2])
            self.pinBottomAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        }
        
        if let pinType = pinTypes[3] {
            margin = marginFrom(margins[3])
            self.pinLeftAnchor(toView: pinView, pinType: pinType, margin: margin, greaterThanOrEqualTo: true)
        }
    }
    
    func pinGreaterThanOrEqual(toView pinView: UIView, pinType pinTypeIn: AAPinType?, margins: [CGFloat?]) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        // pin "all" sides to corresponding sides of pinView
        //
        // pinType defaults to .superview
        // margins is an array of [top, right, bottom, left].
        //
        // margin defaults to 0
        
        var pinType: AAPinType!
        
        if pinTypeIn == nil {
            pinType = .superview
        } else {
            pinType = pinTypeIn
        }
        
        self.pinTopAnchor(toView: pinView, pinType: pinType, margin: margins[0], greaterThanOrEqualTo: true)
        self.pinRightAnchor(toView: pinView, pinType: pinType, margin: margins[1], greaterThanOrEqualTo: true)
        self.pinBottomAnchor(toView: pinView, pinType: pinType, margin: margins[2], greaterThanOrEqualTo: true)
        self.pinLeftAnchor(toView: pinView, pinType: pinType, margin: margins[3], greaterThanOrEqualTo: true)
    }
    
    func pinTypeArrayDesc(_ pinTypeArray: [AAPinType?]) -> String {
        var pinTypeArrayDesc = "["
        var notFirstElement = false
        
        for pinType in pinTypeArray {
            if notFirstElement {
                pinTypeArrayDesc += ", "
            } else {
                notFirstElement = true
            }
            if let pinType = pinType {
                pinTypeArrayDesc += pinType.rawValue
            } else {
                pinTypeArrayDesc += "nil"
            }
        }
        pinTypeArrayDesc += "]"
        
        return pinTypeArrayDesc
    }
    
    func marginFrom(_ marginIn: CGFloat?) -> CGFloat {
        if marginIn == nil {
            return 0
        } else {
            return marginIn!
        }
    }
    
    // MARK: - Pin individual sides
    
    func pinTopAnchor(toView pinView: UIView, pinType: AAPinType, margin: CGFloat?, greaterThanOrEqualTo: Bool = false) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        
        var pinViewTopAnchor: NSLayoutYAxisAnchor!
        
        switch pinType {
        case .superview:
            pinViewTopAnchor = pinView.topAnchor
        case .layoutMargin:
            pinViewTopAnchor = pinView.layoutMarginsGuide.topAnchor
        case .safeArea:
            if #available(iOS 11, *) {
                pinViewTopAnchor = pinView.safeAreaLayoutGuide.topAnchor
            } else {
                pinViewTopAnchor = pinView.topAnchor
            }
        }
        
        if pinViewTopAnchor == nil {
            return
        }
        
        if let margin = margin {
            if greaterThanOrEqualTo {
                self.topAnchor.constraint(greaterThanOrEqualTo: pinViewTopAnchor, constant: margin).isActive = true
            } else {
                self.topAnchor.constraint(equalTo: pinViewTopAnchor, constant: margin).isActive = true
            }
        } else {
            self.topAnchor.constraint(equalTo: pinViewTopAnchor).isActive = true
        }
    }
    
    func pinRightAnchor(toView pinView: UIView, pinType: AAPinType, margin: CGFloat?, greaterThanOrEqualTo: Bool = false) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        
        var pinViewRightAnchor: NSLayoutXAxisAnchor!
        
        switch pinType {
        case .superview:
            pinViewRightAnchor = pinView.rightAnchor
        case .layoutMargin:
            pinViewRightAnchor = pinView.layoutMarginsGuide.rightAnchor
        case .safeArea:
            if #available(iOS 11, *) {
                pinViewRightAnchor = pinView.safeAreaLayoutGuide.rightAnchor
            } else {
                pinViewRightAnchor = pinView.rightAnchor
            }
        }
        
        if pinViewRightAnchor == nil {
            return
        }
        
        if let margin = margin {
            if greaterThanOrEqualTo {
                self.rightAnchor.constraint(greaterThanOrEqualTo: pinViewRightAnchor, constant: -margin).isActive = true
            } else {
                self.rightAnchor.constraint(equalTo: pinViewRightAnchor, constant: -margin).isActive = true
            }
        } else {
            self.rightAnchor.constraint(equalTo: pinViewRightAnchor).isActive = true
        }
    }
    
    func pinBottomAnchor(toView pinView: UIView, pinType: AAPinType, margin: CGFloat?, greaterThanOrEqualTo: Bool = false, priority: Float? = nil) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        
        var pinViewBottomAnchor: NSLayoutYAxisAnchor!
        
        switch pinType {
        case .superview:
            pinViewBottomAnchor = pinView.bottomAnchor
        case .layoutMargin:
            pinViewBottomAnchor = pinView.layoutMarginsGuide.bottomAnchor
        case .safeArea:
            if #available(iOS 11, *) {
                pinViewBottomAnchor = pinView.safeAreaLayoutGuide.bottomAnchor
            } else {
                pinViewBottomAnchor = pinView.bottomAnchor
            }
        }
        
        if pinViewBottomAnchor == nil {
            return
        }
        
        if let margin = margin {
            if greaterThanOrEqualTo {
                let constraint = self.bottomAnchor.constraint(lessThanOrEqualTo: pinViewBottomAnchor, constant: -margin)
                if let priority = priority {
                    constraint.priority = UILayoutPriority(rawValue: priority)
                }
                constraint.isActive = true
                
            } else {
                self.bottomAnchor.constraint(equalTo: pinViewBottomAnchor, constant: -margin).isActive = true
            }
        } else {
            self.bottomAnchor.constraint(equalTo: pinViewBottomAnchor).isActive = true
        }
    }
    
    func pinLeftAnchor(toView pinView: UIView, pinType: AAPinType, margin: CGFloat?, greaterThanOrEqualTo: Bool = false) {
        //Func.ifDebug(print("=== UIView+constraints.\(#function) ==="))
        
        var pinViewLeftAnchor: NSLayoutXAxisAnchor!
        
        switch pinType {
        case .superview:
            pinViewLeftAnchor = pinView.leftAnchor
        case .layoutMargin:
            pinViewLeftAnchor = pinView.layoutMarginsGuide.leftAnchor
        case .safeArea:
            if #available(iOS 11, *) {
                pinViewLeftAnchor = pinView.safeAreaLayoutGuide.leftAnchor
            } else {
                pinViewLeftAnchor = pinView.leftAnchor
            }
        }
        
        if pinViewLeftAnchor == nil {
            return
        }
        
        if let margin = margin {
            if greaterThanOrEqualTo {
                self.leftAnchor.constraint(greaterThanOrEqualTo: pinViewLeftAnchor, constant: margin).isActive = true
            } else {
                self.leftAnchor.constraint(equalTo: pinViewLeftAnchor, constant: margin).isActive = true
            }
        } else {
            self.leftAnchor.constraint(equalTo: pinViewLeftAnchor).isActive = true
        }
    }
}
