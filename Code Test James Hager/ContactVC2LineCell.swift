//
//  ContactVC2LineCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A base class for two-line ContactVCCell that has a single label and a single textField.
//
//  Subclassed by ContactVCDoBCell and ContactVCEmailCell
//  (ContactVCPhoneCell uses a special PhoneNumberTextField)
//

import UIKit
import CoreData

class ContactVC2LineCell: ContactVCCell {
    
    var targetForAction: ContactVC?
    
    var typeLabel: UILabel!
    var dataTextField: UITextField!
    
    var disclosureView: UIView!
    
    var initialLabelID: NSManagedObjectID!
    var initialValue = ""
    
    override var hasChanges: Bool {
        return dataTextField.text != initialValue
    }
    
    // MARK: - Configure
    
    override func setView(dispMode: ContactVCDispMode) {
        super.setView(dispMode: dispMode)
        
        if dispMode == .disp {
            typeLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
            typeLabel.textColor = .darkGray
        } else {
            if typeLabelChangesWithDispMode {
                typeLabel.textColor = Color.avantiGreen
            } else {
                typeLabel.textColor = Color.darkGray
            }
            typeLabel.font = UIFont.preferredFont(forTextStyle: .body)
        }
        
        if typeLabelChangesWithDispMode {
            setLabelText(dispMode: dispMode)
        }
        setTextField(dataTextField, hasAction: dataType.hasTapAction, forDispMode: dispMode)
    }
    
    func setLabelText(dispMode: ContactVCDispMode) {
        let isUserInteractionEnabled = dispMode != .disp
        
        typeLabel.isUserInteractionEnabled = isUserInteractionEnabled
        disclosureView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    override func setViewsIsHidden(dispMode: ContactVCDispMode) {
        disclosureView?.isHidden = dispMode == .disp
    }
    
    // MARK: - Setup View
    
    override func setUpView() {
        typeLabel = makeLabel()
        
        if typeLabelChangesWithDispMode {
            disclosureView = UIView(frame: CGRect.zero)
            setUpTypeLabel(typeLabel, andDisclosureView: disclosureView)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
            tapGestureRecognizer.delegate = self
            typeLabel.addGestureRecognizer(tapGestureRecognizer)
            
            let discTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
            discTapGestureRecognizer.delegate = self
            disclosureView.addGestureRecognizer(discTapGestureRecognizer)
            
        } else {
            typeLabel.add(toView: contentView, pinTypes: [.layoutMargin, .layoutMargin, nil, .layoutMargin])
        }
        
        dataTextField = makeTextField()
        dataTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, .layoutMargin, .layoutMargin])
        dataTextField.pinTop(toBottomOf: typeLabel, margin: 2)
        
        if dataType.hasTapAction {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDataTap))
            tapGestureRecognizer.delegate = self
            dataTextField.addGestureRecognizer(tapGestureRecognizer)
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleContactVCCellShouldResignFirstResponders),
            name: NotificationName.contactVCCellShouldResignFirstResponders,
            object: nil)
        
        super.setUpView()
    }
    
    // MARK: - Actions
    
    override func firstTextFieldBecomeFirstResponder() {
        dataTextField.becomeFirstResponder()
    }
    
    @objc func handleContactVCCellShouldResignFirstResponders() {
        dataTextField.resignFirstResponder()
    }
    
    @objc func handleLabelTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
    }
    
    @objc func handleDataTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
    }
}
