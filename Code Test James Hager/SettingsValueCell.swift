
import UIKit

class SettingsValueCell: UITableViewCell {
    
    let userDefaults = UserDefaults.standard
    var valueName = ""
    var value: AnyObject!
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, valueName: String, valueType: ValueType, values: AnyObject?) {
        self.valueName = valueName
        
        //Func.ifDebug(print("=== \(file).\(#function) - text=\(text), valueType=\(valueType), values=\(values)"))
        
        label.text = text
        
        let valueHelper = SettingsValueHelper(valueName: valueName, values: values!)
        valueLabel.text = "\(valueHelper.getValueToDisplayOfSavedValueForValueName())"
        
        accessoryType = .disclosureIndicator
    }
}
