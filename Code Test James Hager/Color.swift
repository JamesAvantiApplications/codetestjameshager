//
//  Color.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

enum Color {
    static let avantiGreen = UIColor(red: 0, green: 84/255.0, blue: 73/255.0, alpha: 1.0)
    static let lightishGreen = UIColor(red: 191/255.0, green: 212/255.0, blue: 209/255.0, alpha: 1.0)  // 75% white
    static let lightGreen = UIColor(red: 215/255.0, green: 229/255.0, blue: 227/255.0, alpha: 1.0)  // 84% white
    static let lightLightGreen = UIColor(red: 242/255.0, green: 246/255.0, blue: 246/255.0, alpha: 1.0)  // 95% white
    static let mediumGreen = UIColor(red: 56/255.0, green: 119/255.0, blue: 109/255.0, alpha: 1.0)
    static let separator = UIColor(red: 0.49, green: 0.63, blue: 0.60, alpha: 1.0)
    static let lightGray = UIColor(white: 247/255.0, alpha: 1.0)
    static let lightLightGray = UIColor(white: 252/255.0, alpha: 1.0)
    static let darkGray = UIColor(white: 45/255.0, alpha: 1.0)
    //static let grayAvantiGreenL = UIColor(white: 73.86/255.0, alpha: 1.0)
}
