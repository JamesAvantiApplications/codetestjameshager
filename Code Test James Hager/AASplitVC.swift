//
//  AASplitVC.swift
//
//  Created by James Hager on 12/19/16.
//  Copyright © 2016 James Hager. All rights reserved.
//
//  An overall view controller that, in conjunction with the enclosing AAContainerVC, restricts master/detail presentation to iPads.
//

import UIKit

class AASplitVC: UISplitViewController {
    
    var forSingleViewControllerShowPrimaryOnLaunch = false
    
    var mdHelper: MasterDetailHelper!
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        delegate = self
        preferredDisplayMode = .allVisible
        view.backgroundColor = Color.mediumGreen  // the separator color between master and detail
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        mdHelper.isMasterDetail = traitCollection.horizontalSizeClass == .regular
    }
}

// MARK: - UISplitViewControllerDelegate

extension AASplitVC: UISplitViewControllerDelegate {
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        // Return YES to prevent UIKit from applying its default behavior
        return forSingleViewControllerShowPrimaryOnLaunch
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, showDetail vc: UIViewController, sender: Any?) -> Bool {
        return false
    }
    
    func splitViewControllerPreferredInterfaceOrientationForPresentation(_ splitViewController: UISplitViewController) -> UIInterfaceOrientation {
        return UIInterfaceOrientation.portrait
    }
}
