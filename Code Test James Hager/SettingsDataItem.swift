//
//  SettingsDataItem.swift
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import UIKit

class SettingsDataItem {
    
    var dataType: SettingsDataType
    var text: String!
    var textFromClosure: (() -> String)!
    var cellType: SettingsCellIdentifiers
    var color: UIColor!
    var disclosureIndicator: Bool
    var closure: (() -> Void)!
    var segmentedControlName: String!
    var segmentWidth: CGFloat?
    var switchName: String!
    var enabledOnClosure: (() -> Bool)!
    var valueName: String!
    var valueType: ValueType!
    var values: AnyObject!
    
    var imageName: String!
    
    enum SettingsDataType {
        case regular
        case upgradeToFullVersion
        case version
        case segmentedControl
        case `switch`
        case valuePicker
        
        // AboutVC
        case bullet
        case image
    }
    
    convenience init(text: String) {
        self.init(text: text, cellType: .textCell)
    }
    
    convenience init(text: String, closure: (() -> Void)!) {
        self.init(text: text, cellType: .textCell, disclosureIndicator: true, closure: closure)
    }
    
    convenience init(text: String, statusColorBarColor: UIColor) {
        self.init(text: text, cellType: .textCell)
        self.color = statusColorBarColor
    }
    
    init(textFromClosure closure: @escaping (() -> String), cellType: SettingsCellIdentifiers, disclosureIndicator: Bool) {
        self.dataType = .regular
        self.textFromClosure = closure
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
    }
    
    convenience init(dataType: SettingsDataType) {
        switch dataType{
        case .version:
            self.init(dataType: dataType, cellType: .textSmallCell, disclosureIndicator: false)
        default:
            self.init(text: "blank", cellType: .textCell)
        }
    }
    
    init(text: String, segmentedControlName: String, values: AnyObject, segmentWidth: CGFloat?) {
        self.dataType = .segmentedControl
        self.text = text
        self.cellType = .segmentedControlCell
        self.disclosureIndicator = false
        self.segmentedControlName = segmentedControlName
        self.values = values
        self.segmentWidth = segmentWidth
    }
    
    convenience init(text: String, segmentedControlName: String, values: AnyObject, segmentWidth: CGFloat?, enabledOnClosure closure: @escaping (() -> Bool)) {
        self.init(text: text, segmentedControlName: segmentedControlName, values: values, segmentWidth: segmentWidth)
        self.enabledOnClosure = closure
    }
    
    init(text: String, switchName: String) {
        self.dataType = .switch
        self.text = text
        self.cellType = .switchCell
        self.disclosureIndicator = false
        self.switchName = switchName
    }
    
    convenience init(text: String, switchName: String, enabledOnClosure closure: @escaping (() -> Bool)) {
        self.init(text: text, switchName: switchName)
        self.enabledOnClosure = closure
    }
    
    init(text: String, valueName: String, valueType: ValueType, values: AnyObject) {
        self.dataType = .valuePicker
        self.text = text
        self.cellType = .valueCell
        self.disclosureIndicator = true
        self.valueName = valueName
        self.valueType = valueType
        self.values = values
    }
    
    init(text: String, cellType: SettingsCellIdentifiers) {
        self.dataType = .regular
        self.text = text
        self.cellType = cellType
        self.disclosureIndicator = false
    }
    
    init(text: String, cellType: SettingsCellIdentifiers, textColor: UIColor) {
        self.dataType = .regular
        self.text = text
        self.cellType = cellType
        self.color = textColor
        self.disclosureIndicator = false
    }
    
    init(text: String, cellType: SettingsCellIdentifiers, disclosureIndicator: Bool, closure: (() -> Void)!) {
        self.dataType = .regular
        self.text = text
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
        self.closure = closure
    }
    
    init(dataType: SettingsDataType, cellType: SettingsCellIdentifiers, disclosureIndicator: Bool) {
        self.dataType = dataType
        self.cellType = cellType
        self.disclosureIndicator = disclosureIndicator
    }
    
    // AboutVC
    
    init(bulletText: String) {
        self.dataType = .bullet
        self.text = bulletText
        self.cellType = .bullet
        self.disclosureIndicator = false
    }
    
    convenience init(imageName: String, text: String) {
        self.init(imageName: imageName, text: text, cellType: .image)
    }
    
    convenience init(imageName: String, imageTintColor: UIColor, text: String) {
        self.init(imageName: imageName, text: text, cellType: .image)
        self.color = imageTintColor
    }
    
    convenience init(imageSmallName: String, text: String) {
        self.init(imageName: imageSmallName, text: text, cellType: .imageSmall)
    }
    
    convenience init(imageSmallWithHeaderName: String, text: String) {
        self.init(imageName: imageSmallWithHeaderName, text: text, cellType: .imageSmallWithHeader)
    }
    
    init(imageName: String, text: String, cellType: SettingsCellIdentifiers) {
        self.dataType = .image
        self.text = text
        self.cellType = cellType
        self.disclosureIndicator = false
        self.imageName = imageName
    }
}
