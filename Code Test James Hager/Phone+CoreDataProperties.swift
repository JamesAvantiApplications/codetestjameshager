//
//  Phone+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension Phone {
    
    @NSManaged var numberSave: String
    
    @NSManaged var contact: Contact?
    @NSManaged var label: PhoneLabel?
}
