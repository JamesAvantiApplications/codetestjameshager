//
//  UINavigationController+Fade.swift
//  Code Test James Hager
//
//  Created by James Hager on 1/1/19.
//  Copyright © 2019 Avanti Applications, LLC. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func pushViewControllerWithFade(_ vc: UIViewController) {
        Func.ifDebug(print("=== UINavigationController.\(#function) === \(Func.debugDate())"))
        fadeTransition()
        pushViewController(vc, animated: false)
    }
    
    func popViewControllerWithFade() {
        Func.ifDebug(print("=== UINavigationController.\(#function) === \(Func.debugDate())"))
        fadeTransition()
        popViewController(animated: false)
    }
    
    func replaceViewControllerWithFade(_ vc: UIViewController, atIndex index: Int) {
        Func.ifDebug(print("=== UINavigationController.\(#function) === \(Func.debugDate())"))
        
        guard index > -1 && index < viewControllers.count else { return }
        
        fadeTransition()
        viewControllers[index] = vc
    }
    
    func fadeTransition() {
        Func.ifDebug(print("=== UINavigationController.\(#function) === \(Func.debugDate())"))
        
        guard let navView = view else { return }
        
        let snapshot = NavigationHelper.snapshotView(navView)
        
        UIView.animate(withDuration: NavigationHelper.fadeDuration, animations: {
            snapshot.alpha = 0
        }, completion: { _ in
            snapshot.removeFromSuperview()
        })
    }
}
