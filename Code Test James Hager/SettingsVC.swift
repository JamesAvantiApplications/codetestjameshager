//
//  SettingsVC.swift
//
//  Created by James Hager on 11/19/15.
//  Copyright © 2015 James Hager. All rights reserved.
//

import UIKit
import StoreKit
import MessageUI

class SettingsVC: SettingsVCGeneral {
    
    var viewTitle: String!
    var valueName: String!
    var values: [Double: String] = [:]
    var valueValues: [Double] = []
    
    override var file: String {
        return "SettingsVC"
    }
    
    override func initSettingsDataItems() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        var settingsDataItem: SettingsDataItem!
        
        settingsDataItem = SettingsDataItem(dataType: .version)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "About", closure: {self.showAbout()})
        addSettingsDataItem(settingsDataItem)
        
        /*
        settingsDataItem = SettingsDataItem(text: "Recommend AWeather", closure: {self.handleRecommend()})
        addSettingsDataItem(settingsDataItem)
        */
        
        settingsDataItem = SettingsDataItem(text: "Support", closure: {self.showSupport()})
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Legal", closure: {self.showLegal()})
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Create Sample Contacts...", closure: {self.createSampleContacts()})
        addSettingsDataItem(settingsDataItem)
        
        /*
        settingsDataItem = SettingsDataItem(text: "Reset Settings...", closure: {self.handleResetSettings()})
        addSettingsDataItem(settingsDataItem)
        */
    }
    
    // MARK: - VC Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        title = "Settings"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        mdHelper.rollBackDetail()
    }
    
    // MARK: - Action
    
    /*
    func handleRecommend() {
        let url  = URL(string: "itms-apps://itunes.apple.com/us/app/aweather/id1072955115?ls=1&mt=8")
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.open(url!)
        }
    }
    */
    
    func createSampleContacts() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let alert = UIAlertController(title: "Create Sample Contacts",
                                      message: "Would you like to create these sample contacts?\n\n• Alice Jones\n• Bob Jackson\n• Carlos Gómez\n\nEach will have \"[sample]\" in an address entry so you can search for them.",
                                      preferredStyle: .alert)
        
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(noAction)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in UserDefaultsHelper.createSampleContacts(mdHelper: self.mdHelper)
        }
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func handleResetSettings() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let alert = UIAlertController(title: "Reset Settings",
            message: "Reset the settings to the defaut values?",
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "Reset", style: .destructive) { _ in self.resetSettings()
        }
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func resetSettings() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        UserDefaultsHelper.setUserDefaults()
        tableView.reloadData()
    }

    // MARK: - UITableViewDataSource

    // MARK: - UITableViewDelegate
    
    // MARK: - Navigation
    
    func showAbout() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let aboutVC = AboutVC()
        aboutVC.mdHelper = mdHelper
        aboutVC.isMaster = false
        
        mdHelper.presentViewControllerOnDetail(aboutVC)
    }
    
    func showSupport() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if !MFMailComposeViewController.canSendMail() {
            let alert = UIAlertController(title: "Error", message: "Your device doesn't support Mail messaging.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            return
        }
        
        let subject = AppInfo.appNameWithVersion
        let body = "Feature request or bug report?"
        let recipients = ["Avanti Applications Support <support@avantiapplications.com>"]
        
        let supportVC = MailComposeHelper.makeComposer(delegate: self)
        supportVC.setSubject(subject)
        supportVC.setMessageBody(body, isHTML: true)
        supportVC.setToRecipients(recipients)
        
        present(supportVC, animated: true)
    }
    
    func showLegal() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let legalVC = LegalVC()
        legalVC.mdHelper = mdHelper
        
        mdHelper.presentViewControllerOnDetail(legalVC)
    }
}

// MARK: - MFMailComposeViewControllerDelegate

extension SettingsVC: MFMailComposeViewControllerDelegate {

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if error != nil {
            Func.ifDebug(print("Error: \(String(describing: error))"))
        }
        
        controller.dismiss(animated: true, completion: nil)
        
        if let alert = MailComposeHelper.alert(forResult: result) {
            present(alert, animated: true, completion: nil)
        }
    }
}
