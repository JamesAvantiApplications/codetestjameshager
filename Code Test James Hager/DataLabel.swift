//
//  DataLabel.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/19/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Base class for ContactData labels: AddressLabel, EmailLabel, PhoneLabel
//
//  name is like "home", "work", "other"
//
//  sort values < 0 are app-generated default labels
//              > 0 are user-generated labels
//

import Foundation
import CoreData

@objc(DataLabel)
class DataLabel: NSManagedObject {
    
    func set(name: String, sort: Int16) {
        self.name = name
        self.sort = sort
    }
    
    func describe() {
        print("=== Label.\(#function) === \(Func.debugDate())")
        print("--- - name: '\(name)'")
        print("--- - sort: \(sort)")
    }
}
