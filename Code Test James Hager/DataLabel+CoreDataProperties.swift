//
//  DataLabel+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/19/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension DataLabel {
    
    @NSManaged var name: String
    @NSManaged var sort: Int16
}
