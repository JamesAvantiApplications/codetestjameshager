import UIKit

class LegalCell: UITableViewCell {
    
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ itemInfo: [String]) {
        itemLabel.text = itemInfo[0]
        descLabel.text = itemInfo[1]
    }
}
