//
//  AppDelegate.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var mdHelper = MasterDetailHelper()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Func.ifDebug(print("=================================================================="))
        Func.ifDebug(print("=== \(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("=================================================================="))
        
        customizeAppearance()
        //setUserDefaultsIfNeeded()
        createInitialDataIfNeeded()
        
        let aaContainerVC = self.window!.rootViewController as! AAContainerVC
        aaContainerVC.mdHelper = mdHelper
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Misc Methods
    
    func customizeAppearance() {
        Func.ifDebug(print("=== \(#function) ==="))
        
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = Color.mediumGreen
        
        let whiteImage = UIImage.imageOfColor(.white, size: CGSize(width: 1.0, height: 1.0))
        UINavigationBar.appearance().setBackgroundImage(whiteImage, for: .default)
        UINavigationBar.appearance().shadowImage = whiteImage
        
        UITextField.appearance().tintColor = Color.avantiGreen
    }
    
    func setUserDefaultsIfNeeded() {
        Func.ifDebug(print("=== \(#function) ==="))
        
        let userDefaults = UserDefaults.standard
        let userDefaultsCreated = userDefaults.bool(forKey: "UserDefaultsCreated")
        
        if !userDefaultsCreated {
            userDefaults.set(true, forKey: "UserDefaultsCreated")
            
            let dataStateKey = "DataState"
            let dataState = 1
            userDefaults.set(dataState, forKey: dataStateKey)
            
            UserDefaultsHelper.setUserDefaultSettings(userDefaults: userDefaults)
            userDefaults.synchronize()
        }
    }
    
    func createInitialDataIfNeeded() {
        Func.ifDebug(print("=== \(#function) ==="))
        
        guard let _ = AddressLabel.label(withName: "home", context: mdHelper.coreDataStack.context)
            else {
                InitialDataHelper.createInitialData(context: mdHelper.coreDataStack.context)
                mdHelper.coreDataStack.saveContext()
                return
        }
    }
    
    /*
    func describeLabels() {
        print("====================")
        print("=== \(#function) ===")
        print("====================")

        let addressLabels = AddressLabel.labels(context: mdHelper.coreDataStack.context)
        describeLabels(addressLabels, name: "addressLabels")
        
        let emailLabels = EmailLabel.labels(context: mdHelper.coreDataStack.context)
        describeLabels(emailLabels, name: "emailLabels")
        
        let phoneLabels = PhoneLabel.labels(context: mdHelper.coreDataStack.context)
        describeLabels(phoneLabels, name: "phoneLabels")
    }
    
    func describeLabels(_ labels: [DataLabel], name: String) {
        print("--- \(#function) --- \(name) ---")
        for label in labels {
            print("--- - sort: \(label.sort), name: '\(label.name)'")
        }
    }
    */
}
