//
//  LegalVC.swift
//
//  Created by James Hager on 11/24/15.
//  Copyright © 2015 James Hager. All rights reserved.
//

import UIKit

class LegalVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    var navSeparator: UIView!
    var tableView: UITableView!
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    struct TableViewCellIdentifiers {
        static let legalCell = "LegalCell"
    }
    
    let itemInfo: [Int: [String]] = [
        0: ["Graphics Icons", "\nSome icons are from www.flaticon.com and were designed by Gregor Cresnar and Freepik."],
        1: ["PhoneNumberKit", "\nCopyright (c) 2018 Roy Marmelstein\n\nPermission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:\n\nThe above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."]
        ]

    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        title = "Legal"
        
        let views = Func.makeNavSeparatorAndTableView(inView: view, dataSource: self, delegate: self)
        navSeparator = views.navSeparator
        tableView = views.tableView

        let cellNib = UINib(nibName: TableViewCellIdentifiers.legalCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.legalCell)
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.separatorColor = Color.separator
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        Func.ifDebug(print("-----------------------------------------"))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("-----------------------------------------"))
        
        if mdHelper.isMasterDetail {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}

// MARK: - UITableViewDataSource

extension LegalVC: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemInfo.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Func.ifDebug(print("=== \(file).\(#function) \(displayPath(indexPath)) === \(displayPath(indexPath)) ==="))
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.legalCell, for: indexPath) as! LegalCell
        cell.configureCell(itemInfo[indexPath.row]!)
        return cell
    }
}

// MARK: - UITableViewDelegate
    
extension LegalVC: UITableViewDelegate {
        
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        //Func.ifDebug(print("=== \(file).\(#function) \(displayPath(indexPath)) === \(displayPath(indexPath)) ==="))
        
        return nil
    }
}
