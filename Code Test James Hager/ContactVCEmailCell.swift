//
//  ContactVCEmailCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC Email Cell
//

import UIKit
import CoreData

class ContactVCEmailCell: ContactVC2LineCell {
    
    var email: Email!
    
    var initialSort: Int16!
    
    override var hasChanges: Bool {
        if let curID = email.label?.objectID, let iniID = initialLabelID {
            return super.hasChanges || sortHasChanges || curID != iniID
        } else {
            return super.hasChanges || sortHasChanges
        }
    }
    
    var sortHasChanges: Bool {
        return email.sort != initialSort
    }
    
    override var idForChange: NSObject {
        return email.objectID
    }
    
    var emailLabels: [EmailLabel]!
    
    // MARK: - Configure
    
    func configure(email: Email, dispMode: ContactVCDispMode, delegate: ContactVCCellDelegate) {
        dataType = .email
        
        self.email = email
        configure(delegate: delegate)
        
        initialLabelID = email.label?.objectID
        initialValue = email.addr
        initialSort = email.sort
        
        if typeLabel == nil {
            setUpView()
        }
        
        setView(dispMode: dispMode)
        
        typeLabel.text = email.label?.name ?? "label"
        dataTextField.text = initialValue
        dataTextField.placeholder = "Email"
        dataTextField.keyboardType = .emailAddress
        dataTextField.autocapitalizationType = .none
    }
    
    // MARK: - Actions
    
    @objc override func handleLabelTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCEmailCell.\(#function) ==="))
        
        emailLabels = EmailLabel.labels(context: delegate!.returnContext())
        dataLabelPickerIndexOfSelectedValue = emailLabels.firstIndex(where: {$0 == email.label})!
        
        showDataLabelPickerVC(values: emailLabels.map({$0.name}), delegate: self)
    }
    
    @objc override func handleDataTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCEmailCell.\(#function) ==="))
        
        guard email.addr.count > 0 else { return }
        
        delegate?.sendEmailTo(email)
    }
    
    override func handleTextFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== ContactVCEmailCell.\(#function) ==="))
        
        email.addr = textField.text!
        
        checkHasChangesHasChanged()
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCEmailCell {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        delegate?.setNextFirstResponder(dataType: dataType, contactData: email)
        
        return true
    }
}

// MARK: - DataLabelPickerVCDelegate

extension ContactVCEmailCell: DataLabelPickerVCDelegate {
    
    func newIndexOfSelectedValue(_ index: Int) {
        Func.ifDebug(print("=== ContactVCEmailCell.\(#function) === index: \(index), dataLabelPickerIndexOfSelectedValue: \(dataLabelPickerIndexOfSelectedValue!)"))
        
        if index != dataLabelPickerIndexOfSelectedValue {
            email.label = emailLabels[index]
            typeLabel.text = email.label!.name
            
            checkHasChangesHasChanged()
        }
    }
}
