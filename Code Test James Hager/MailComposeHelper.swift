//
//  MailComposeHelper.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A namespace for MFMailCompose functions
//

import Foundation
import MessageUI

enum MailComposeHelper {
    
    static func makeComposer(delegate: MFMailComposeViewControllerDelegate) -> MFMailComposeViewController {
        
        let mcVC = MFMailComposeViewController()
        mcVC.mailComposeDelegate = delegate
        mcVC.navigationBar.tintColor = Color.mediumGreen
        mcVC.navigationBar.isTranslucent = false
        return mcVC
    }
    
    static func alert(forResult result: MFMailComposeResult) -> UIAlertController? {
        
        var alertTitle: String!
        var alertMessage: String!
        
        switch result {
        case .failed:
            alertTitle = "Message Failed"
            
        case .saved:
            alertTitle = "Message Saved"
            alertMessage = "The message was saved in your Drafts folder."
            
        case .sent:
            alertTitle = "Message Sent"
            alertMessage = "The message was queued to your Outbox and will be sent as soon as possible."
            
        default:  // cancelled
            break
        }
        
        guard alertTitle != nil else { return nil }
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        return alert
    }
}
