//
//  SearchStringHelper.swift
//
//  Created by James Hager on 11/26/16.
//  Copyright © 2016 James Hager. All rights reserved.
//
//  A helper class to convert searchBar text to an NSPredicate
//

import Foundation

open class SearchStringHelper {
    
    var inWord = false
    var and = false
    var not = false
    var match = false
    var pieceChars = [Character]()
    
    var firstPredicateComponent = true
    var predicate: NSPredicate!
    
    open var searchTermsToHighlight = [String]()
    open var searchTermsToNotHighlight = [String]()
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    func resetParams() {
        inWord = false
        and = false
        not = false
        match = false
        pieceChars = [Character]()
    }
    
    open func predicateFromSearchString(_ string: String) -> NSPredicate? {
        Func.ifDebug(print("=== \(file).\(#function) === string: '\(string)' ==="))
        
        var index = 0
        let indexMax = string.count - 1
        let stringStartIndex = string.startIndex
        var stringIndex = stringStartIndex
        var char = string[stringIndex]
        
        while index < string.count {
            stringIndex = string.index(stringStartIndex, offsetBy: index)
            char = string[stringIndex]
            //Func.ifDebug(print("--- \(file).\(#function) - char='\(char)'"))
            if !inWord && char == "+" {
                and = true
            } else if !inWord && char == "-" {
                not = true
            } else if !inWord && char == "\"" {
                inWord = true
                match = true
            } else if char == "\"" && match {
                setPredicate()
            } else if char == " " && !match {
                if pieceChars.count > 0 {
                    setPredicate()
                }
            } else if index == indexMax {
                pieceChars.append(char)
                setPredicate()
            } else {
                inWord = true
                pieceChars.append(char)
            }
            index += 1
        }
        
        #if DEBUG
        for searchTerm in searchTermsToHighlight {
            print("--- \(file).\(#function) - searchTermToHighlight = '\(searchTerm)'")
        }
        for searchTerm in searchTermsToNotHighlight {
            print("--- \(file).\(#function) - searchTermToNotHighlight = '\(searchTerm)'")
        }
        #endif
        
        return predicate
    }
    
    func setPredicate() {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        
        let string = String(pieceChars).lowercased()
        
        if not {
            searchTermsToNotHighlight.append(string)
        } else {
            searchTermsToHighlight.append(string)
        }
        
        Func.ifDebug(print("--- \(file).\(#function) - string: '\(string)', and: \(and), not: \(not), match: \(match)"))
        
        var notString: String!
        var contains: String!
        
        if not {
            notString = "NOT "
        } else {
            notString = ""
        }
        
        contains = "CONTAINS '" + string + "'"
        
        //Func.ifDebug(print("--- \(file).\(#function) - contains = '\(contains!)'"))
        var predicateString: String!
        
        if match {
            predicateString = String(format: "%@searchTextMatch %@", notString!, contains!)
        } else {
            predicateString = String(format: "%@searchTextUnique %@", notString!, contains!)
        }
        
        let newPredicateComponent = NSPredicate(format: predicateString)
        
        Func.ifDebug(print("--- \(file).\(#function) - newPredicateComponent: '\(newPredicateComponent)'"))
        
        if firstPredicateComponent {
            predicate = newPredicateComponent
            firstPredicateComponent = false
            
        } else {
            if and || not {
                predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate, newPredicateComponent])
                
            } else {
                predicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate, newPredicateComponent])
            }
        }
        
        Func.ifDebug(print("--- \(file).\(#function) - predicate: '\(predicate!)'"))
        
        resetParams()
    }
}
