//
//  EmailLabel.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(EmailLabel)
class EmailLabel: DataLabel {
    
    convenience init(name: String, sort: Int16, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.emailLabel.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        set(name: name, sort: sort)
    }
    
    static func labels(context: NSManagedObjectContext, sortAscending: Bool = true) -> [EmailLabel] {
        var items = [EmailLabel]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName.emailLabel.rawValue)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "sort", ascending: sortAscending)]
        
        do {
            items = try context.fetch(fetchRequest) as! [EmailLabel]
            
        } catch let error as NSError {
            Func.ifDebug(print("*** Could not fetch \(error), \(error.userInfo)"))
        }
        
        return items
    }
    
    static func label(withName name: String, context: NSManagedObjectContext) -> EmailLabel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName.emailLabel.rawValue)
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        
        do {
            let items = try context.fetch(fetchRequest) as! [EmailLabel]
            if items.count > 0 {
                return items[0]
            }
            
        } catch let error as NSError {
            Func.ifDebug(print("*** Could not fetch \(error), \(error.userInfo)"))
        }
        
        return nil
    }
}
