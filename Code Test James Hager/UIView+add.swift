//
//  UIView+add.swift
//
//  Created by James Hager on 8/20/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  UIView extensions to add a subview to a superview (called on the subview)
//
//  pinTypes and margins are arrays of [top, right, bottom, left]
//

import UIKit

extension UIView {
    
    func addWithoutPinning(toView parentView: UIView) {        
        self.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(self)
    }
    
    func addWithCenterConstraints(toView parentView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(self)
        
        self.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: parentView.centerYAnchor).isActive = true
    }
    
    // Add to view and pin to that view
    
    func add(toView parentView: UIView, pinType: AAPinType? = nil, margin: CGFloat? = nil) {
        var pinTypes: [AAPinType?]!
        
        if let pinType = pinType {
            pinTypes = [pinType, pinType, pinType, pinType]
        } else {
            pinTypes = [.superview, .superview, .superview, .superview]
        }
        
        add(toView: parentView, pinTypes: pinTypes, margin: margin)
    }
    
    func add(toView parentView: UIView, pinTypes: [AAPinType?], margin: CGFloat? = nil) {
        let margins: [CGFloat?] = [margin, margin, margin, margin]
        add(toView: parentView, pinTypes: pinTypes, margins: margins)
    }
    
    func add(toView parentView: UIView, pinType: AAPinType? = nil, margins: [CGFloat?]) {
        var pinTypes: [AAPinType?]!
        
        if let pinType = pinType {
            pinTypes = [pinType, pinType, pinType, pinType]
        } else {
            pinTypes = [.superview, .superview, .superview, .superview]
        }
        
        add(toView: parentView, pinTypes: pinTypes, margins: margins)
    }
    
    func add(toView parentView: UIView, pinTypes: [AAPinType?], margins: [CGFloat?]) {
        add(toView: parentView, andPinTo: parentView, pinTypes: pinTypes, margins: margins)
    }
    
    // Add to view and pin to a different view
    
    func add(toView parentView: UIView, andPinTo pinView: UIView, pinType: AAPinType? = nil, margin: CGFloat? = nil) {
        
        if pinType == nil && margin == nil {
            self.translatesAutoresizingMaskIntoConstraints = false
            parentView.addSubview(self)
            
            self.topAnchor.constraint(equalTo: pinView.topAnchor).isActive = true
            self.bottomAnchor.constraint(equalTo: pinView.bottomAnchor).isActive = true
            self.leftAnchor.constraint(equalTo: pinView.leftAnchor).isActive = true
            self.rightAnchor.constraint(equalTo: pinView.rightAnchor).isActive = true
            
            return
        }
        
        var pinTypes: [AAPinType?]!
        
        if let pinType = pinType {
            pinTypes = [pinType, pinType, pinType, pinType]
        } else {
            pinTypes = [.superview, .superview, .superview, .superview]
        }
        
        add(toView: parentView, andPinTo: pinView, pinTypes: pinTypes, margin: margin)
    }
    
    func add(toView parentView: UIView, andPinTo pinView: UIView, pinTypes: [AAPinType?], margin: CGFloat? = nil) {
        let margins: [CGFloat?] = [margin, margin, margin, margin]
        add(toView: parentView, andPinTo: pinView, pinTypes: pinTypes, margins: margins)
    }
    
    func add(toView parentView: UIView, andPinTo pinView: UIView, pinTypes: [AAPinType?], margins: [CGFloat?]) {
        self.translatesAutoresizingMaskIntoConstraints = false
        parentView.addSubview(self)
        
        self.pin(toView: pinView, pinTypes: pinTypes, margins: margins)
    }
}
