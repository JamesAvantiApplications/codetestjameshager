//
//  String+trimmedLeadingWhitespaces.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/29/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension String {
    
    func trimmedLeadingWhitespaces() -> String {
        // trim leading whitespaces
        
        guard self.count > 0 else { return self }
        
        var trimmedString = self
        while let whitespaceRange = trimmedString.rangeOfCharacter(from: .whitespaces, options: [], range: nil) {
            if whitespaceRange.lowerBound == trimmedString.startIndex {
                trimmedString = trimmedString.replacingCharacters(in: whitespaceRange, with: "")
            } else {
                break
            }
        }
        return trimmedString
    }
}
