//
//  Address.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData
import Contacts

@objc(Address)
class Address: ContactData {
    
    var city: String {
        get { return citySave }
        set {
            citySave = newValue
            contact?.setSearchText()
        }
    }
    
    var country: String {
        get { return countrySave }
        set {
            countrySave = newValue
            contact?.setSearchText()
        }
    }
    
    var state: String {
        get { return stateSave }
        set {
            stateSave = newValue
            contact?.setSearchText()
        }
    }
    
    var street1: String {
        get { return street1Save }
        set {
            street1Save = newValue
            contact?.setSearchText()
        }
    }
    
    var street2: String {
        get { return street2Save }
        set {
            street2Save = newValue
            contact?.setSearchText()
        }
    }
    
    var zip: String {
        get { return zipSave }
        set {
            zipSave = newValue
            contact?.setSearchText()
        }
    }
    
    // MARK: - Other Variables
    
    var hasData: Bool {
        return street1.count > 0
            || street1.count > 0
            || street2.count > 0
            || city.count > 0
            || state.count > 0
            || zip.count > 0
            || country.count > 0
    }
    
    var multiLineString: String {
        return formattedString(lineSep: "\n", cityStateSep: ", ")
    }
    
    var singleLineString: String {
        return formattedString(lineSep: ", ", cityStateSep: ", ")
    }
    
    var searchTextMatchString: String {
        return formattedString(lineSep: " ", cityStateSep: " ")
    }
    
    var oneString: String {
        var string = street1
        string += " " + street2
        string += " " + city
        string += " " + state
        string += " " + zip
        string += " " + country
        return string
    }
    
    var addressDictionary: [String : String] {
        return makeAddressDictionary()
    }
    
    // MARK: - Create/Delete Methods
    
    convenience init(label: AddressLabel?, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.address.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        self.street1 = ""
        self.street2 = ""
        self.city = ""
        self.state = ""
        self.zip = ""
        self.country = ""
        self.sort = -1
        self.label = label
    }
    
    convenience init(street1: String, street2: String, city: String, state: String, zip: String, country: String, label: AddressLabel?, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.address.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        self.street1 = street1
        self.street2 = street2
        self.city = city
        self.state = state
        self.zip = zip
        self.country = country
        self.label = label
    }
    
    // MARK: - Alternate Formats
    
    func formattedString(lineSep: String, cityStateSep: String) -> String {
        // lineSep is the character to put between normal lines of the address
        //   - "\n" for multi-line display
        //   - ", " for single-line display and map format
        //   - " " for searchTextMatch
        
        // cityStateSep is the character to put between the city and state
        //   - ", " for regular display and map format
        //   - " " for searchTextMatch
        
        var formattedString = ""
        
        var firstLineEntry = true
        var firstCityStateZip = true
        
        if street1.count > 0 {
            firstLineEntry = false
            formattedString += street1
        }
        
        if street2.count > 0 {
            if firstLineEntry {
                firstLineEntry = false
            } else {
                formattedString += lineSep
            }
            formattedString += street2
        }
        
        if city.count > 0 {
            if firstLineEntry {
                firstLineEntry = false
            } else {
                formattedString += lineSep
            }
            firstCityStateZip = false
            formattedString += city
        }
        
        if state.count > 0 {
            if firstCityStateZip {
                firstCityStateZip = false
                if firstLineEntry {
                    firstLineEntry = false
                } else {
                    formattedString += lineSep
                }
            } else {
                formattedString += cityStateSep
            }
            formattedString += state
        }
        
        if zip.count > 0 {
            if firstCityStateZip {
                firstCityStateZip = false
                if firstLineEntry {
                    firstLineEntry = false
                } else {
                    formattedString += lineSep
                }
            } else {
                formattedString += " "
            }
            formattedString += zip
        }
        
        if country.count > 0 {
            if firstLineEntry {
                firstLineEntry = false
            } else {
                formattedString += lineSep
            }
            formattedString += country
        }
        
        return formattedString
    }
    
    func makeAddressDictionary() -> [String : String] {
        // make an address dictionary for an MKPlacemark
        
        var addrDict = [String : String]()
        
        var street = ""
        var firstLineEntry = true
        
        if street1.count > 0 {
            firstLineEntry = false
            street = street1
        }
        
        if street2.count > 0 {
            if !firstLineEntry {
                street += ", "
            }
            street += street2
        }
        
        if street.count > 0 {
            addrDict[CNPostalAddressStreetKey] = street
        }
        
        if city.count > 0 {
            addrDict[CNPostalAddressCityKey] = city
        }
        
        if state.count > 0 {
            addrDict[CNPostalAddressStateKey] = state
        }
        
        if zip.count > 0 {
            addrDict[CNPostalAddressPostalCodeKey] = zip
        }
        
        if country.count > 0 {
            addrDict[CNPostalAddressCountryKey] = country
        }
        
        return addrDict
    }
    
    // MARK: - Describe
    
    func describe() {
        print("=== Address.\(#function) === \(Func.debugDate())")
        print("--- - street1: '\(street1)'")
        print("--- - street2: '\(street2)'")
        print("--- - .. city: '\(city)'")
        print("--- - . state: '\(state)'")
        print("--- - ... zip: '\(zip)'")
        print("--- - country: '\(country)'")
        print("--- - .. sort: \(sort)")
        
        if let contact = contact {
            print("--- > contact: '\(contact.nameNatural)'")
        }
        
        label?.describe()
    }
}
