//
//  String+DataValidation.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension String {
    
    var isEmailAddress: Bool {
        // based on https://developer.bombbomb.com/blog/2017/10/24/swift-data-detection/
        
        if let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) {
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let match = matches.first {
                if let matchURL = match.url,
                    let matchURLComponents = URLComponents(url: matchURL, resolvingAgainstBaseURL: false),
                    matchURLComponents.scheme == "mailto"
                {
                    let address = String(matchURLComponents.path)
                    return address.count == self.count
                }
            }
        }
        return false
    }
    
    var isPhoneNumber: Bool {
        // based on https://stackoverflow.com/questions/36789896/check-the-valid-phone-number
        
        if let detector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue) {
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.range.location == 0 && res.range.length == self.count
            }
        }
        return false
    }
}
