//
//  PhoneLabel+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension PhoneLabel {
    
    @NSManaged var phones: Set<Phone>
}
