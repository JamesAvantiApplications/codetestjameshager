//
//  NavigationHelper.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/31/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A namespace for navigation helper functions
//

import UIKit

enum NavigationHelper {
    
    static let fadeDuration: TimeInterval = 0.2
    
    static func snapshotView(_ view: UIView) -> UIView {
        // take a snapshot of the view, make it a subview, then return it
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let snapshotView = UIImageView(image: image)
        
        view.addSubview(snapshotView)
        
        return snapshotView
    }
}
