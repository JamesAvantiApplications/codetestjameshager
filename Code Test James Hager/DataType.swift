//
//  DataType.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/19/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum DataType: String, CustomStringConvertible {
    case name
    case phone
    case email
    case address
    case dateOfBirth
    
    var description: String { return rawValue }
    
    var hasTapAction: Bool {
        switch self {
        case .phone, .email, .address:
            return true
        default:
            return false
        }
    }
    
    var contactVCSection: Int {
        switch self {
        case .name:
            return 0
        case .phone:
            return 1
        case .email:
            return 2
        case .address:
            return 3
        case .dateOfBirth:
            return 4
        }
    }
}
