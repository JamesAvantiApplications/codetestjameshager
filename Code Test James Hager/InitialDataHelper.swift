//
//  InitialDataHelper.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/19/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A namespace for initial data functions
//

import Foundation
import CoreData

class InitialDataHelper {
    
    static func createInitialData(context: NSManagedObjectContext) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        createInitialAddressLabels(context: context)
        createInitialEmailLabels(context: context)
        createInitialPhoneLabels(context: context)
    }
    
    static func createInitialAddressLabels(context: NSManagedObjectContext) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        let names = ["home","work","other"]
        var sort: Int16 = -50
        
        for name in names {
            let _ = AddressLabel(name: name, sort: sort, context: context)
            sort += 1
        }
    }
    
    static func createInitialEmailLabels(context: NSManagedObjectContext) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        let names = ["home","work","other"]
        var sort: Int16 = -50
        
        for name in names {
            let _ = EmailLabel(name: name, sort: sort, context: context)
            sort += 1
        }
    }
    
    static func createInitialPhoneLabels(context: NSManagedObjectContext) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        let names = ["home","work","mobile","main","home fax","work fax","pager","other"]
        var sort: Int16 = -50
        
        for name in names {
            let _ = PhoneLabel(name: name, sort: sort, context: context)
            sort += 1
        }
    }
}
