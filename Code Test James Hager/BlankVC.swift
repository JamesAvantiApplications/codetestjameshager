//
//  BlankVC.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class BlankVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    //var navSeparator: UIView!
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        view.backgroundColor = .white
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //navSeparator = Func.makeNavSeparator(inView: view)
    }
}
