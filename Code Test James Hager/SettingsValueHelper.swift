//
//  SettingsValueHelper.swift
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import Foundation

class SettingsValueHelper {
    //
    // helper to handle SettingsValues that are either an array or an array-of-arrays
    //
    // the array is a list of values which are both the valueToSave and the valueToDisplay
    // the array-of-arrays is a list of values that are [valueToSave, valueToDisplay]
    //
    
    var valueName: String!
    var values: AnyObject!
    var valuesDataType: SettingsValueDataType = .array
    
    var mainArray: [AnyObject]!
    
    lazy var valueType: ValueType = {
        switch self.valuesDataType {
        case .array:
            let value = self.mainArray[0]
            return self.valueTypeFromValue(value)
            
        case .arrayOfArrays:
            let subArray = self.mainArray[0]
            let value = subArray[0]
            return self.valueTypeFromValue(value as AnyObject)
        }
        
    }()
    
    lazy var userDefaults: UserDefaults = UserDefaults.standard
    
    enum SettingsValueDataType {
        case array
        case arrayOfArrays
    }
    
    init(valueName: String, values: AnyObject) {
        self.valueName = valueName
        self.values = values
        
        // check if array of arrays
        if let array = values as? NSArray {
            mainArray = array as [AnyObject]
            if let _ = array[0] as? NSArray {
                valuesDataType = .arrayOfArrays
            }
        }
        
        //Func.ifDebug(print("SettingsValueHelper.valuesDataType = \(valuesDataType)"))
    }
    
    func valueTypeFromValue(_ value: AnyObject) -> ValueType {
        if let _ = value as? String {
            return .string
            
        } else if let _ = value as? Double {
            return .double
            
        } else {
            return .int
            
        }
    }
    
    func valueSaveForIndex(_ index: Int) -> AnyObject {
        switch valuesDataType {
        case .array:
            return mainArray[index]
        case .arrayOfArrays:
            let subArray = mainArray[index] as! [AnyObject]
            return subArray[0]
        }
    }
    
    func valueShowForIndex(_ index: Int) -> AnyObject {
        switch valuesDataType {
        case .array:
            return mainArray[index]
        case .arrayOfArrays:
            let subArray = mainArray[index] as! [AnyObject]
            return subArray[1]
        }
    }
    
    func valueShowForKey(_ key: AnyObject) -> AnyObject? {
        switch valuesDataType {
        case .array:
            return key
        case .arrayOfArrays:
            switch valueType {
            case .string:
                let key = key as! String
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index] as! [AnyObject]
                    if subArray[0] as! String == key {
                        return subArray[1]
                    }
                }
                
            case .double:
                let key = key as! Double
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index] as! [AnyObject]
                    if subArray[0] as! Double == key {
                        return subArray[1]
                    }
                }
                
            case .int:
                let key = key as! Int
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index] as! [AnyObject]
                    if subArray[0] as! Int == key {
                        return subArray[1]
                    }
                }
            }
            
            return nil
        }
    }
    
    func indexForKey(_ key: AnyObject) -> Int? {
        switch valuesDataType {
        case .array:
            return (mainArray as NSArray).index(of: key)
        case .arrayOfArrays:
            if let key = key as? String {
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index]
                    if subArray[0] as! String == key {
                        return index
                    }
                }
            } else if let key = key as? Double {
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index]
                    if subArray[0] as! Double == key {
                        return index
                    }
                }
                
            } else if let key = key as? Int {
                for index in (0..<mainArray.count) {
                    let subArray = mainArray[index]
                    if subArray[0] as! Int == key {
                        return index
                    }
                }
            }
            
            return nil
        }
    }
    
    func valuesToDisplay() -> [AnyObject] {
        switch valuesDataType {
        case .array:
            return mainArray
        case .arrayOfArrays:
            var values = [AnyObject]()
            for index in (0..<mainArray.count) {
                let valueArray = mainArray[index] as! [AnyObject]
                values.append(valueArray[1])
            }
            return values
        }
    }
    
    func getSavedValueForValueName() -> AnyObject {
        switch valueType {
        case .double:
            return userDefaults.double(forKey: valueName) as AnyObject
        case .int:
            return userDefaults.integer(forKey: valueName) as AnyObject
        case .string:
            return userDefaults.string(forKey: valueName)! as AnyObject
        }
    }
    
    func getValueToDisplayOfSavedValueForValueName() -> AnyObject {
        return valueShowForKey(getSavedValueForValueName())!
    }
    
    func getIndexOfSavedValueForValueName() -> Int {
        return indexForKey(getSavedValueForValueName())!
    }
    
    func saveValueForIndex(_ index: Int) {
        let valueToSave = valueSaveForIndex(index)
        
        switch valueType {
        case .double: userDefaults.set(valueToSave as! Double, forKey: valueName)
        case .int: userDefaults.set(valueToSave as! Int, forKey: valueName)
        case .string: userDefaults.set(valueToSave as! String, forKey: valueName)
        }
        
        userDefaults.synchronize()
    }
}
