//
//  DataLabelPickerVC.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/22/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

protocol DataLabelPickerVCDelegate {
    func newIndexOfSelectedValue(_ index: Int)
}

// MARK: -

class DataLabelPickerVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    var viewTitle: String!
    var values: [String]!
    var indexOfSelectedValue: Int!
    
    var delegate: DataLabelPickerVCDelegate?
    
    var navSeparator: UIView!
    var tableView: UITableView!
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    struct TableViewCellIdentifier {
        static let settingsValueViewCell = "SettingsValueViewCell"
    }
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        setUpView()
        
        let cellNib = UINib(nibName: TableViewCellIdentifier.settingsValueViewCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifier.settingsValueViewCell)
        
        tableView.separatorColor = Color.separator
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        tableView.flashScrollIndicators()
    }
    
    // MARK: - Setup
    
    func setUp(viewTitle: String, values: [String], indexOfSelectedValue: Int, delegate: DataLabelPickerVCDelegate) {
        self.viewTitle = viewTitle
        self.values = values
        self.indexOfSelectedValue = indexOfSelectedValue
        self.delegate = delegate
    }
    
    func setUpView() {
        navigationItem.title = viewTitle
        
        let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        navigationItem.leftBarButtonItem = cancelBarButton
        
        let views = Func.makeNavSeparatorAndTableView(inView: view, dataSource: self, delegate: self)
        navSeparator = views.navSeparator
        tableView = views.tableView
    }
    
    // MARK: - Actions
    
    @objc func cancel() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        mdHelper.removeViewController()
    }
}

// MARK: - UITableViewDataSource

extension DataLabelPickerVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.settingsValueViewCell, for: indexPath) as! SettingsValueViewCell
        cell.configureCell(text: values[indexPath.row], showChecked: indexPath.row == indexOfSelectedValue)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension DataLabelPickerVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        delegate?.newIndexOfSelectedValue(indexPath.row)
        
        mdHelper.removeViewController()
    }
}
