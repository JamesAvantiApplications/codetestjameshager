//
//  Email.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(Email)
class Email: ContactData {
    
    var addr: String {
        get { return addrSave }
        set {
            addrSave = newValue
            contact?.setSearchText()
        }
    }
    
    // MARK: - Other Variables

    var hasData: Bool {
        return addr.count > 0
    }
    
    // MARK: - Create/Delete Methods
    
    convenience init(label: EmailLabel?, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.email.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        self.addr = ""
        self.label = label
        self.sort = -1
    }
    
    convenience init(addr: String, label: EmailLabel?, context: NSManagedObjectContext) {
        self.init(label: label, context: context)
        self.addr = addr
    }
    
    // MARK: - Describe
    
    func describe() {
        print("=== Email.\(#function) === \(Func.debugDate())")
        print("--- - .. addr: '\(addr)'")
        print("--- - .. sort: \(sort)")
        
        if let contact = contact {
            print("--- > contact: '\(contact.nameNatural)'")
        }
        
        label?.describe()
    }
}
