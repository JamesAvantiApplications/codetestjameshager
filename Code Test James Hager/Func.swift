//
//  Func.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A namespace for miscellaneous functions
//

import UIKit

enum Func {
    
    static func afterDelay(_ seconds: Double, closure: @escaping () -> ()) {
        let when = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    static func debugDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: Date())
    }
    
    static func displayPath(_ inputPath: IndexPath?) -> String {
        if let path = inputPath {
            let nsPath = path as NSIndexPath
            let length = path.count
            var pathString = "["
            var first = true
            for position in (0..<length) {
                if first {
                    first = false
                } else {
                    pathString += ", "
                }
                pathString += "\(nsPath.index(atPosition: position))"
            }
            
            pathString += "]"
            return pathString
        } else {
            return "nil"
        }
    }
    
    static func ifDebug(_ closure: @autoclosure () -> Void) {
        // Used for only printing "if DEBUG"
        // The autoclosure closure is only evaluated if it's executed
        #if DEBUG
        closure()
        #endif
    }
    
    static func getSourceFileNameFromFullPath(_ file: String) -> String {
        let fileComponents1 = file.components(separatedBy: "/")
        let lastComponent1 = fileComponents1.last!
        let fileComponents2 = lastComponent1.components(separatedBy: ".")
        let firstComponent2 = fileComponents2.first!
        return firstComponent2
    }
    
    static func makeNavSeparator() -> UIView {
        let navSeparator = UIView(frame: CGRect.zero)
        navSeparator.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        navSeparator.backgroundColor = Color.mediumGreen
        return navSeparator
    }
    
    static func makeNavSeparator(inView view: UIView) -> UIView {
        let navSeparator = makeNavSeparator()
        navSeparator.add(toView: view, pinTypes: [.superview, .superview, nil, .superview], margins: [1, nil, nil, nil])
        return navSeparator
    }
    
    static func makeNavSeparatorAndTableView(inView view: UIView, dataSource: UITableViewDataSource, delegate: UITableViewDelegate) -> (navSeparator: UIView, tableView: UITableView) {
        
        let navSeparator = makeNavSeparator(inView: view)
        
        let tableView = UITableView(frame: CGRect.zero)
        tableView.add(toView: view, pinTypes: [nil, .superview, .superview, .superview])
        tableView.pinTop(toBottomOf: navSeparator)
        tableView.dataSource = dataSource
        tableView.delegate = delegate
        
        return (navSeparator, tableView)
    }
}
