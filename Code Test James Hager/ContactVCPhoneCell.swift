//
//  ContactVCPhoneCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC Phone Cell
//
//  Uses PhoneNumberTextField from PhoneNumberKit to auto-format phone numbers
//

import UIKit
import CoreData
import PhoneNumberKit

class ContactVCPhoneCell: ContactVCCell {
    
    var phone: Phone!
    
    var typeLabel: UILabel!
    var dataTextField: PhoneNumberTextField!
    
    var disclosureView: UIView!
    
    var initialLabelID: NSManagedObjectID!
    var initialValue = ""
    var initialSort: Int16!
    
    override var hasChanges: Bool {
        if let curID = phone.label?.objectID, let iniID = initialLabelID {
            return valueOrSortHasChanges || curID != iniID
        } else {
            return valueOrSortHasChanges
        }
    }
    
    var valueOrSortHasChanges: Bool {
        return dataTextField.text != initialValue || phone.sort != initialSort
    }
    
    override var idForChange: NSObject {
        return phone.objectID
    }
    
    var phoneLabels: [PhoneLabel]!
    
    // MARK: - Configure
    
    func configure(phone: Phone, dispMode: ContactVCDispMode, delegate: ContactVCCellDelegate) {
        dataType = .phone
        
        self.phone = phone
        configure(delegate: delegate)
        
        initialLabelID = phone.label?.objectID
        initialValue = phone.number
        initialSort = phone.sort
        
        if typeLabel == nil {
            setUpView()
        }
        
        setView(dispMode: dispMode)
        
        typeLabel.text = phone.label?.name ?? "label"
        dataTextField.text = initialValue
        dataTextField.placeholder = "Phone"
        dataTextField.keyboardType = .phonePad
    }
    
    override func setView(dispMode: ContactVCDispMode) {
        super.setView(dispMode: dispMode)
        
        if dispMode == .disp {
            typeLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
            typeLabel.textColor = .darkGray
        } else {
            typeLabel.font = UIFont.preferredFont(forTextStyle: .body)
            typeLabel.textColor = Color.avantiGreen
        }
        
        setLabelText(dispMode: dispMode)
        setTextField(dataTextField, hasAction: dataType.hasTapAction, forDispMode: dispMode)
    }
    
    func setLabelText(dispMode: ContactVCDispMode) {
        let isUserInteractionEnabled = dispMode != .disp
        
        typeLabel.isUserInteractionEnabled = isUserInteractionEnabled
        disclosureView.isUserInteractionEnabled = isUserInteractionEnabled
    }
    
    override func setViewsIsHidden(dispMode: ContactVCDispMode) {
        disclosureView?.isHidden = dispMode == .disp
    }
    
    // MARK: - Setup View
    
    override func setUpView() {
        typeLabel = makeLabel()
        disclosureView = UIView(frame: CGRect.zero)
        setUpTypeLabel(typeLabel, andDisclosureView: disclosureView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
        tapGestureRecognizer.delegate = self
        typeLabel.addGestureRecognizer(tapGestureRecognizer)
        
        let discTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
        discTapGestureRecognizer.delegate = self
        disclosureView.addGestureRecognizer(discTapGestureRecognizer)
        
        dataTextField = PhoneNumberTextField()
        dataTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, .layoutMargin, .layoutMargin])
        dataTextField.pinTop(toBottomOf: typeLabel, margin: 2)
        
        setUpTextField(dataTextField, placeholder: "Phone")
        
        let dataTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDataTap))
        dataTapGestureRecognizer.delegate = self
        dataTextField.addGestureRecognizer(dataTapGestureRecognizer)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleContactVCCellShouldResignFirstResponders),
            name: NotificationName.contactVCCellShouldResignFirstResponders,
            object: nil)
        
        super.setUpView()
    }
    
    // MARK: - Actions
    
    override func firstTextFieldBecomeFirstResponder() {
        dataTextField.becomeFirstResponder()
    }
    
    @objc func handleContactVCCellShouldResignFirstResponders() {
        dataTextField.resignFirstResponder()
    }
    
    @objc func handleLabelTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCPhoneCell.\(#function) ==="))
        
        phoneLabels = PhoneLabel.labels(context: delegate!.returnContext())
        dataLabelPickerIndexOfSelectedValue = phoneLabels.firstIndex(where: {$0 == phone.label})!
        
        showDataLabelPickerVC(values: phoneLabels.map({$0.name}), delegate: self)
    }
    
    @objc func handleDataTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCPhoneCell.\(#function) ==="))
        
        guard phone.number.count > 0 else { return }
        
        delegate?.placeCallTo(phone)
    }
    
    override func handleTextFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== ContactVCPhoneCell.\(#function) ==="))
        
        phone.number = textField.text!
        
        checkHasChangesHasChanged()
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCPhoneCell {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        delegate?.setNextFirstResponder(dataType: dataType, contactData: phone)
        
        return true
    }
}

// MARK: - DataLabelPickerVCDelegate

extension ContactVCPhoneCell: DataLabelPickerVCDelegate {
    
    func newIndexOfSelectedValue(_ index: Int) {
        Func.ifDebug(print("=== ContactVCPhoneCell.\(#function) === index: \(index), dataLabelPickerIndexOfSelectedValue: \(dataLabelPickerIndexOfSelectedValue!)"))
        
        if index != dataLabelPickerIndexOfSelectedValue {
            phone.label = phoneLabels[index]
            typeLabel.text = phone.label!.name
            
            checkHasChangesHasChanged()
        }
    }
}
