//
//  ContactData+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension ContactData {
    
    @NSManaged var sort: Int16
}
