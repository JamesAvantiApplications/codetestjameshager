//
//  String+removeLeadingTrailingNonAlphaNumeric.swift
//  ANotes
//
//  Created by James Hager on 9/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension String {
    
    mutating func removeLeadingTrailingNonAlphaNumeric() {
        // remove leading and trailing non-alphanumeric characters
        self.removeLeadingNonAlphaNumeric()
        self.removeTrailingNonAlphaNumeric()
    }
    
    mutating func removeLeadingNonAlphaNumeric() {
        // remove leading non-alphanumeric characters
        //Func.ifDebug(print("=== \(#function) === '\(self)'"))
        
        let alphaNumeric = CharacterSet.alphanumerics
        
        let startIndex = self.startIndex
        var indexCheck = 0
        var index = startIndex
        while indexCheck < self.count {
            index = self.index(startIndex, offsetBy: indexCheck)
            let char = self[index]
            //Func.ifDebug(print("indexCheck: \(indexCheck), char: '\(char)'"))
            let testSet = CharacterSet(charactersIn: String(char))
            if testSet.isSubset(of: alphaNumeric) {
                break
            }
            indexCheck += 1
        }
        
        if indexCheck > 0 {
            self = String(self[index..<self.endIndex])
        }
        //Func.ifDebug(print("--- \(#function) --- '\(self)'"))
    }
    
    mutating func removeTrailingNonAlphaNumeric() {
        // remove trailing non-alphanumeric characters
        //Func.ifDebug(print("=== \(#function) === '\(self)'"))
        
        let alphaNumeric = CharacterSet.alphanumerics
        
        let startIndex = self.startIndex
        var indexCheck = self.count - 1
        var index = startIndex
        while indexCheck > 0 {
            index = self.index(startIndex, offsetBy: indexCheck)
            let char = self[index]
            //Func.ifDebug(print("indexCheck: \(indexCheck), char: '\(char)'"))
            let testSet = CharacterSet(charactersIn: String(char))
            if testSet.isSubset(of: alphaNumeric) {
                break
            }
            indexCheck -= 1
        }
        
        if indexCheck > 0 {
            self = String(self[startIndex...index])
        }
        //Func.ifDebug(print("--- \(#function) --- '\(self)'"))
    }
}
