//
//  Contact.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  searchTextMatch is for "exact-text" searches
//  searchTextUnique is for partial matches and includes only unique words
//

import Foundation
import CoreData

@objc(Contact)
class Contact: NSManagedObject {
    
    var dateOfBirth: Date? {
        get { return dateOfBirthSave }
        set {
            dateOfBirthSave = newValue
            setSearchText()
        }
    }
        
    var firstName: String {
        get { return firstNameSave }
        set {
            firstNameSave = newValue
            setSearchText()
            setSortData()
        }
    }
    
    var lastName: String {
        get { return lastNameSave }
        set {
            lastNameSave = newValue
            setSearchText()
            setSortData()
        }
    }
    
    // MARK: - Other Variables
    
    var nameNatural: String {
        if firstName.count > 0 && lastName.count > 0 {
            return firstName + " " + lastName
        } else {
            return namePartial
        }
    }
    
    var nameLastFirst: String {
        if firstName.count > 0 && lastName.count > 0 {
            return lastName + ", " + firstName
        } else {
            return namePartial
        }
    }
    
    var namePartial: String {
        if firstName.count > 0 {
            return firstName
        } else if lastName.count > 0 {
            return lastName
        } else {
            return "[no name]"
        }
    }
    
    var dateOfBirthString: String {
        guard let dob = dateOfBirth else {
            return ""
        }
        
        return dateString(forDate: dob)
    }
    
    lazy var dobDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        return dateFormatter
    }()
    
    // MARK: - Create/Delete Methods
    
    convenience init(context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.contact.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        self.firstName = ""
        self.lastName = ""
        setSearchText()
        setSortData()
    }
    
    convenience init(firstName: String, lastName: String, dateOfBirth: Date?, addresses: Set<Address>?, emails: Set<Email>?, phones: Set<Phone>?, context: NSManagedObjectContext) {
        self.init(context: context)
        
        self.firstName = firstName
        self.lastName = lastName
        self.dateOfBirth = dateOfBirth
        self.addresses = addresses
        self.emails = emails
        self.phones = phones
        setSearchText()
        setSortData()
    }
    
    func setUpNew(context: NSManagedObjectContext) {
        
        let addressLabels = AddressLabel.labels(context: context)
        let emailLabels = EmailLabel.labels(context: context)
        let phoneLabels = PhoneLabel.labels(context: context)
        
        addAddress(Address(label: addressLabels[0], context: context))
        addEmail(Email(label: emailLabels[0], context: context))
        addPhone(Phone(label: phoneLabels[0], context: context))
    }
    
    func deleteSelf(context: NSManagedObjectContext) {
        
        if let addresses = addresses {
            for address in addresses {
                context.delete(address)
            }
        }
        
        if let emails = emails {
            for email in emails {
                context.delete(email)
            }
        }
        
        if let phones = phones {
            for phone in phones {
                context.delete(phone)
            }
        }
        
        context.delete(self)
    }
    
    // MARK: - Add/Delete ContactData Methods
    
    func addAddress(_ address: Address) {
        address.sort = sortForNewData(addresses)
        address.contact = self
    }
    
    func addEmail(_ email: Email) {
        email.sort = sortForNewData(emails)
        email.contact = self
    }
    
    func addPhone(_ phone: Phone) {
        phone.sort = sortForNewData(phones)
        phone.contact = self
    }
    
    func sortForNewData(_ contactDataSet: Set<ContactData>?) -> Int16 {
        if let contactDataSet = contactDataSet, contactDataSet.count > 0 {
            return contactDataSet.map{$0.sort}.max()! + 1
        }
        return 0
    }
    
    func deleteAddress(_ address: Address, context: NSManagedObjectContext) {
        addresses?.remove(address)
        deleteContactData(address, context: context)
    }
    
    func deleteEmail(_ email: Email, context: NSManagedObjectContext) {
        emails?.remove(email)
        deleteContactData(email, context: context)
    }
    
    func deletePhone(_ phone: Phone, context: NSManagedObjectContext) {
        phones?.remove(phone)
        deleteContactData(phone, context: context)
    }
    
    private func deleteContactData(_ contactData: ContactData, context: NSManagedObjectContext) {
        context.delete(contactData)
        setSearchText()
    }
    
    // MARK: - SearchText Methods
    
    func setSearchText() {
        //Func.ifDebug(print("=== Contact.\(#function) === \(Func.debugDate())"))
        //  searchTextMatch is for "exact-text" searches
        //  searchTextUnique is for partial matches and includes only unique words
        
        var matchParts = [String]()
        
        // for the name, allow a match for "firstName lastName" and "lastName, firstName"
        
        if firstName.count > 0 && lastName.count > 0 {
            matchParts.append(searchTextMatch(inputString: nameNatural))
            matchParts.append(searchTextMatch(inputString: nameLastFirst))
        } else {
            matchParts.append(searchTextMatch(inputString: namePartial))
        }
        
        if dateOfBirth != nil {
            matchParts.append(searchTextMatch(inputString: dateOfBirthString))
        }
        
        if let addresses = addresses {
            for address in addresses {
                matchParts.append(searchTextMatch(inputString: address.searchTextMatchString))
            }
        }
        
        if let emails = emails {
            for email in emails {
                matchParts.append(searchTextMatch(inputString: email.addr))
            }
        }
        
        if let phones = phones {
            for phone in phones {
                matchParts.append(searchTextMatch(inputString: phone.number))
            }
        }
        
        // join the match terms with an atypical character to prevent matches across data items
        
        var separator = "\u{001F}"  // unicode INFORMATION SEPARATOR ONE
        
        searchTextMatch = matchParts.joined(separator: separator)
        //Func.ifDebug(print("--- \(#function) - searchTextMatch --> ---> -->\n\(searchTextMatch)\n<--- <--- <---"))
        
        separator = " "
        searchTextUnique = searchTextUnique(inputString: matchParts.joined(separator: separator))
        //Func.ifDebug(print("--- \(#function) - searchTextUnique --> ---> -->\n\(searchTextUnique)\n<--- <--- <---"))
    }
    
    func searchTextMatch(inputString: String) -> String {
        // create search text to match
        //   - make lowercase, remove diacriticals
        //   - remove carriage returns
        //   - remove extra spaces
        
        // make case and diacritic insensitive, and remove carriage returns
        var modString = inputString.folding(options: [.caseInsensitive, .diacriticInsensitive, .widthInsensitive], locale: nil).replacingOccurrences(of: "\n", with: " ")
        
        // remove extra spaces
        while modString.contains("  ") {
            modString = modString.replacingOccurrences(of: "  ", with: " ")
        }
        
        if modString.hasPrefix(" ") {
            modString = String(modString.dropFirst(1))
        }
        
        if modString.hasSuffix(" ") {
            modString = String(modString.dropLast(1))
        }
        
        return modString
    }
    
    func searchTextUnique(inputString: String) -> String {
        // create search text with only unique words
        
        let wordsSet = Set(inputString.components(separatedBy: " "))
        
        return wordsSet.joined(separator: " ")
    }
    
    func wordsForSearch(inputString: String) -> String {
        // create a unique set of words from inputString
        
        // make case insensitive and remove carriage returns
        let modString = inputString.folding(options: [.caseInsensitive, .widthInsensitive], locale: nil).replacingOccurrences(of: "\n", with: " ")
        
        // split into "words"
        var wordsSet = Set(modString.components(separatedBy: " "))
        
        // add stripped-out diacritics
        let wordsString = wordsSet.joined(separator: " ")
        let diacriticInsensitive = wordsString.folding(options: .diacriticInsensitive, locale: nil)
        let diacriticInsensitiveSet = Set(diacriticInsensitive.components(separatedBy: " "))
        
        wordsSet.formUnion(diacriticInsensitiveSet)
        
        // add removed leading/trailing non-alphanumeric
        for var word in wordsSet {
            word.removeLeadingTrailingNonAlphaNumeric()
            wordsSet.insert(word)
        }
        
        // add hyphenated parts
        for word in wordsSet {
            let wordParts = word.components(separatedBy: "-")
            if wordParts.count > 0 {
                for wordPart in wordParts {
                    wordsSet.insert(wordPart)
                }
            }
        }
        
        return wordsSet.joined(separator: " ")
    }
    
    // MARK: - Sort Methods
    
    func setSortData() {
        // set sort and sectionKey
        
        // sort
        
        sort = nameLastFirst
        
        // sectionKey
        
        if lastName.count > 0 {
            sectionKey = String(lastName.prefix(1)).uppercased()
            return
        }
        if firstName.count > 0 {
            sectionKey = String(firstName.prefix(1)).uppercased()
            return
        }
        sectionKey = "[]"
    }
    
    // MARK: - Misc Methods
    
    func dateString(forDate date: Date) -> String {
        return dobDateFormatter.string(from: date)
    }
    
    // MARK: - Describe
    
    func describe() {
        print("=== Contact.\(#function) === \(Func.debugDate())")
        
        print("--- - . firstName: '\(firstName)'")
        print("--- - .. lastName: '\(lastName)'")
        print("--- - dateOfBirth: '\(dateOfBirthString)'")
        
        if let phoneSet = phones {
            var phones = Array(phoneSet)
            phones.sort(by: { $0.sort < $1.sort })
            for phone in phones {
                phone.describe()
            }
        }
        
        if let emailSet = emails {
            var emails = Array(emailSet)
            emails.sort(by: { $0.sort < $1.sort })
            for email in emails {
                email.describe()
            }
        }
        
        if let addrSet = addresses {
            var addrs = Array(addrSet)
            addrs.sort(by: { $0.sort < $1.sort })
            for addr in addrs {
                addr.describe()
            }
        }
    }
}
