//
//  ContactData.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/21/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Base class for ContactData: Address, Email, and Phone
//

import Foundation
import CoreData

@objc(ContactData)
class ContactData: NSManagedObject {
    
}
