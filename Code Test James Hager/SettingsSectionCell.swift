
import UIKit

class SettingsSectionCell: UITableViewCell {
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    @IBOutlet weak var sectionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ text: String, textColor: UIColor?) {
        //Func.ifDebug(print("=== \(file).\(#function) - text=\(text) *** ***"))
        selectionStyle = .none
        
        sectionLabel.text = text
        
        if let textColor = textColor {
            backgroundColor = .clear
            sectionLabel.textColor = textColor
        } else {
            backgroundColor = Color.lightGreen
            sectionLabel.textColor = .black
        }
    }
}
