//
//  ContactVCDispMode.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC display mode
//

import Foundation

enum ContactVCDispMode: String, CustomStringConvertible {
    case disp
    case add
    case edit
    
    var description: String { return rawValue }
    
    var navTitle: String? {
        switch self {
        case .disp:
            return nil
        case .add:
            return "Add Contact"
        case .edit:
            return "Edit"
        }
    }
}
