//
//  SettingsValueVC.swift
//
//  Created by James Hager on 11/20/15.
//  Copyright © 2015 James Hager. All rights reserved.
//

import UIKit

protocol SettingsValueViewControllerDelegate {
    func reloadSettingsRow(at: IndexPath)
}

// MARK: -

class SettingsValueVC: UITableViewController {
    
    let userDefaults = UserDefaults.standard
    
    var mdHelper: MasterDetailHelper!
    
    var delegate: SettingsValueViewControllerDelegate?
    var indexPathInSettingsVC: IndexPath!
    
    var viewTitle: String!
    var valueName: String!
    var valueType: ValueType!
    var values: AnyObject!
    var valuesToDisplay: [AnyObject]!
    var indexOfSavedValue: Int!
    
    var valuesHelper: SettingsValueHelper!
    
    lazy var value: AnyObject = self.valuesHelper.getSavedValueForValueName()
    
    var selectedIndexPath = IndexPath()
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    struct TableViewCellIdentifiers {
        static let settingsValueViewCell = "SettingsValueViewCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        let cellNib = UINib(nibName: TableViewCellIdentifiers.settingsValueViewCell, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: TableViewCellIdentifiers.settingsValueViewCell)
        
        navigationItem.title = viewTitle
        
        valuesHelper = SettingsValueHelper(valueName: valueName, values: values)
        valuesToDisplay = valuesHelper.valuesToDisplay()
        
        #if DEBUG
        for value in valuesToDisplay {
            print("--- \(file).\(#function) - valuesToDisplay: '\(value)'")
        }
        #endif
        
        indexOfSavedValue = valuesHelper.getIndexOfSavedValueForValueName()
        Func.ifDebug(print("--- \(file).\(#function) - indexOfSavedValue: \(indexOfSavedValue!)"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if mdHelper.isMasterDetail {
            let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
            navigationItem.leftBarButtonItem = cancelBarButton
            
        } else {
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        }
    }
    
    // MARK: - Actions
    
    @objc func cancel() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        mdHelper.popSettingsDetail()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.settingsValueViewCell, for: indexPath) as! SettingsValueViewCell
        cell.label?.text = "\(valuesToDisplay[indexPath.row])"
        
        if indexPath.row == indexOfSavedValue {
            selectedIndexPath = indexPath
            cell.accessoryType = .checkmark
        }
        
        cell.selectionStyle = .default
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        if indexPath.row != selectedIndexPath.row {
            valuesHelper.saveValueForIndex(indexPath.row)
        }
        
        navigationController?.popViewController(animated: true)
    }
}
