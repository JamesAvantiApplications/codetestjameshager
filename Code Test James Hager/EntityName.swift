//
//  EntityName.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum EntityName: String, CustomStringConvertible {
    
    case address = "Address"
    case addressLabel = "AddressLabel"
    case contact = "Contact"
    case email = "Email"
    case emailLabel = "EmailLabel"
    case phone = "Phone"
    case phoneLabel = "PhoneLabel"
    
    var description: String { return rawValue }
}
