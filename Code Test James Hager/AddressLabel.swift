//
//  AddressLabel.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(AddressLabel)
class AddressLabel: DataLabel {
    
    convenience init(name: String, sort: Int16, context: NSManagedObjectContext) {
        let entity = NSEntityDescription.entity(forEntityName: EntityName.addressLabel.rawValue, in: context)
        self.init(entity: entity!, insertInto: context)
        set(name: name, sort: sort)
    }
    
    static func labels(context: NSManagedObjectContext, sortAscending: Bool = true) -> [AddressLabel] {
        var items = [AddressLabel]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName.addressLabel.rawValue)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "sort", ascending: sortAscending)]
        
        do {
            items = try context.fetch(fetchRequest) as! [AddressLabel]
            
        } catch let error as NSError {
            Func.ifDebug(print("*** Could not fetch \(error), \(error.userInfo)"))
        }
        
        return items
    }
    
    static func label(withName name: String, context: NSManagedObjectContext) -> AddressLabel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName.addressLabel.rawValue)
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        
        do {
            let items = try context.fetch(fetchRequest) as! [AddressLabel]
            if items.count > 0 {
                return items[0]
            }
            
        } catch let error as NSError {
            Func.ifDebug(print("*** Could not fetch \(error), \(error.userInfo)"))
        }
        
        return nil
    }
}
