//
//  ContactVCSectionHeaderView.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit

class ContactVCSectionHeaderView: UITableViewHeaderFooterView {
    
    var separatorColorViewLeftConstraint: NSLayoutConstraint!
    
    convenience init(dispMode: ContactVCDispMode, separatorInset: CGFloat) {
        self.init()

        contentView.backgroundColor = .white
        
        let separatorColorView = UIView(frame: CGRect.zero)
        separatorColorView.add(toView: self, pinTypes: [.superview, .superview, .superview, nil])
        separatorColorViewLeftConstraint = separatorColorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: separatorInset)
        separatorColorViewLeftConstraint.isActive = true
        
        if dispMode == .disp {
            separatorColorView.backgroundColor = Color.separator
        } else {
            separatorColorView.backgroundColor = Color.mediumGreen
        }
    }
    
    func setSeparatorInset(_ separatorInset: CGFloat) {
        separatorColorViewLeftConstraint.isActive = false
        separatorColorViewLeftConstraint.constant = separatorInset
        separatorColorViewLeftConstraint.isActive = true
    }
}
