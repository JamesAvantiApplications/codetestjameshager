
import UIKit

class SettingsImageTextCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var theImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(_ imageName: String, text: String, withDisclosureIndicator: Bool = false) {
        theImage.image = UIImage(named: imageName)
        configCommon(text: text, withDisclosureIndicator: withDisclosureIndicator)
    }
    
    func configureCell(_ imageName: String, tintColor: UIColor, text: String, withDisclosureIndicator: Bool = false) {
        theImage.image = UIImage(named: imageName)
        theImage.image = theImage.image?.withRenderingMode(.alwaysTemplate)
        theImage.tintColor = tintColor
        
        configCommon(text: text, withDisclosureIndicator: withDisclosureIndicator)
    }
    
    fileprivate func configCommon(text: String, withDisclosureIndicator: Bool) {
        label.text = text
        if withDisclosureIndicator {
            accessoryType = .disclosureIndicator
        } else {
            selectionStyle = .none
        }
    }
}
