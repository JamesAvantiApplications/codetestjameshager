//
//  ContactVCAddCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/19/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  The ContactVC "Add" Cell
//

import UIKit

class ContactVCAddCell: ContactVCCell {
    
    var typeLabel: UILabel!
    
    func configure(dataType: DataType, delegate: ContactVCCellDelegate) {
        self.dataType = dataType
        configure(delegate: delegate)
        
        if typeLabel == nil {
            setUpView()
        }
        
        typeLabel.text = "add \(dataType)"
    }
    
    override func setUpView() {
        typeLabel = makeLabel(textStyle: .body)
        typeLabel.add(toView: contentView, pinTypes: [.layoutMargin, .layoutMargin, .layoutMargin, .layoutMargin], margins: [nil, nil, 6, nil])
        typeLabel.textColor = Color.avantiGreen
    }
}
