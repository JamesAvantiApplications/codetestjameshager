//
//  ContactVCAddrCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC Address Cell
//

import UIKit
import CoreData

class ContactVCAddrCell: ContactVCCell {
    
    var addr: Address!
    
    var typeLabel: UILabel!
    var addrLabel: UILabel!
    
    var street1TextField: UITextField!
    var street2TextField: UITextField!
    var cityTextField: UITextField!
    var stateTextField: UITextField!
    var zipTextField: UITextField!
    var countryTextField: UITextField!
    
    var disclosureView: UIView!
    
    var stateTextFieldWidthConstraint: NSLayoutConstraint!
    
    var initialLabelID: NSManagedObjectID!
    var initialStreet1 = ""
    var initialStreet2 = ""
    var initialCity = ""
    var initialState = ""
    var initialZip = ""
    var initialCountry = ""
    
    var initialSort: Int16!
    
    override var hasChanges: Bool {
        if let curID = addr?.label?.objectID, let iniID = initialLabelID {
            return addrHasChanges || sortHasChanges || curID != iniID
        } else {
            return addrHasChanges || sortHasChanges
        }
    }
    
    var addrHasChanges: Bool {
        if street1TextField == nil {
            return false
        }
        return street1TextField.text != initialStreet1
            || street2TextField.text != initialStreet2
            || cityTextField.text != initialCity
            || stateTextField.text != initialState
            || zipTextField.text != initialZip
            || countryTextField.text != initialCountry
    }
    
    var sortHasChanges: Bool {
        return addr.sort != initialSort
    }
    
    override var idForChange: NSObject {
        return addr.objectID
    }
    
    var addrLabels: [AddressLabel]!
    
    // MARK: - Configure
    
    func configure(addr: Address, dispMode: ContactVCDispMode, delegate: ContactVCCellDelegate) {
        dataType = .address
        
        self.addr = addr
        configure(delegate: delegate)
        
        initialLabelID = addr.label?.objectID
        initialStreet1 = addr.street1
        initialStreet2 = addr.street2
        initialCity = addr.city
        initialState = addr.state
        initialZip = addr.zip
        initialCountry = addr.country
        initialSort = addr.sort
        
        if typeLabel == nil {
            setUpView(dispMode: dispMode)
        } else {
            if dispMode == .disp && addrLabel == nil {
                setUpViewForDisp()
            } else if dispMode != .disp && street1TextField == nil {
                setUpViewForEdit()
            }
        }
        
        setView(dispMode: dispMode)
        
        typeLabel.text = addr.label?.name ?? "label"
        
        if dispMode == .disp {
            addrLabel.text = addr.multiLineString
        } else {
            street1TextField.text = addr.street1
            street2TextField.text = addr.street2
            cityTextField.text = addr.city
            stateTextField.text = addr.state
            zipTextField.text = addr.zip
            countryTextField.text = addr.country
        }
    }
    
    override func setView(dispMode: ContactVCDispMode) {
        
        if dispMode == .disp {
            typeLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        } else {
            typeLabel.font = UIFont.preferredFont(forTextStyle: .body)
        }
        
        if dispMode != .disp && typeLabelChangesWithDispMode {
            typeLabel.textColor = Color.avantiGreen
            typeLabel.isUserInteractionEnabled = true
        } else {
            typeLabel.textColor = .darkGray
            typeLabel.isUserInteractionEnabled = false
        }
        
        super.setView(dispMode: dispMode)
    }
    
    override func setViewsIsHidden(dispMode: ContactVCDispMode) {
        let isHiddenToApply = dispMode == .disp
        
        addrLabel?.isHidden = !isHiddenToApply
        
        street1TextField?.isHidden = isHiddenToApply
        street2TextField?.isHidden = isHiddenToApply
        cityTextField?.isHidden = isHiddenToApply
        stateTextField?.isHidden = isHiddenToApply
        zipTextField?.isHidden = isHiddenToApply
        countryTextField?.isHidden = isHiddenToApply
        
        disclosureView?.isHidden = isHiddenToApply
    }
    
    // MARK: - Setup View
    
    func setUpView(dispMode: ContactVCDispMode) {
        
        typeLabel = makeLabel()
        disclosureView = UIView(frame: CGRect.zero)
        setUpTypeLabel(typeLabel, andDisclosureView: disclosureView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
        tapGestureRecognizer.delegate = self
        typeLabel.addGestureRecognizer(tapGestureRecognizer)
        
        let discTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleLabelTap))
        discTapGestureRecognizer.delegate = self
        disclosureView.addGestureRecognizer(discTapGestureRecognizer)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleContactVCCellShouldResignFirstResponders),
            name: NotificationName.contactVCCellShouldResignFirstResponders,
            object: nil)
        
        if dispMode == .disp {
            setUpViewForDisp()
        } else {
            setUpViewForEdit()
        }
    }
    
    func setUpViewForDisp() {
        addrLabel = makeLabel(textStyle: .body)
        addrLabel.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        addrLabel.pinTop(toBottomOf: typeLabel, margin: 2)
        addrLabel.numberOfLines = 0
        addrLabel.textColor = Color.avantiGreen
        
        let addrLabelToBottom = addrLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor)
        constraintsForDisp.append(addrLabelToBottom)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleAddressTap))
        tapGestureRecognizer.delegate = self
        addrLabel.addGestureRecognizer(tapGestureRecognizer)
        addrLabel.isUserInteractionEnabled = true
        
        super.setUpView()
    }
    
    func setUpViewForEdit() {
        street1TextField = makeTextField(placeholder: "Street")
        street1TextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        street1TextField.pinTop(toBottomOf: typeLabel, margin: 2)
        street1TextField.autocapitalizationType = .words
        
        street2TextField = makeTextField(placeholder: "Street")
        street2TextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        street2TextField.pinTop(toBottomOf: street1TextField, margin: 2)
        street2TextField.autocapitalizationType = .words
        
        cityTextField = makeTextField(placeholder: "City")
        cityTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        cityTextField.pinTop(toBottomOf: street2TextField, margin: 2)
        cityTextField.autocapitalizationType = .words
        
        stateTextField = makeTextField(placeholder: "State/Province")
        stateTextField.add(toView: contentView, pinTypes: [nil, nil, nil, .layoutMargin])
        stateTextFieldWidthConstraint = stateTextField.widthAnchor.constraint(equalToConstant: 200)
        stateTextFieldWidthConstraint.isActive = true
        stateTextField.pinTop(toBottomOf: cityTextField, margin: 2)
        stateTextField.autocapitalizationType = .allCharacters
        
        zipTextField = makeTextField(placeholder: "ZIP/Postal Code")
        zipTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, nil])
        zipTextField.pinLeft(toRightOf: stateTextField, margin: 6)
        zipTextField.pinTop(toBottomOf: cityTextField, margin: 2)
        zipTextField.keyboardType = .numbersAndPunctuation
        
        countryTextField = makeTextField(placeholder: "Country")
        countryTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        countryTextField.pinTop(toBottomOf: stateTextField, margin: 2)
        countryTextField.autocapitalizationType = .words
        
        let countryTextFieldToBottom = countryTextField.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor)
        constraintsForAddEdit.append(countryTextFieldToBottom)
        
        setTextField(street1TextField, forDispMode: .edit)
        setTextField(street2TextField, forDispMode: .edit)
        setTextField(cityTextField, forDispMode: .edit)
        setTextField(stateTextField, forDispMode: .edit)
        setTextField(zipTextField, forDispMode: .edit)
        setTextField(countryTextField, forDispMode: .edit)
        
        super.setUpView()
    }
    
    func updateConstraintsForNewSizes() {
        // set the stateTextField width constraint so that it's:
        //   - more than 1/3 the width of the cityTextField
        //   - hopefully bigger than the placeholder width
        //   - less than 1/2 the width of the cityTextField
        
        stateTextFieldWidthConstraint.isActive = false
        
        let cityWidth = contentView.frame.width - contentView.layoutMargins.left - contentView.layoutMargins.right
        
        let stateWidth = min(max(0.33 * cityWidth - 3, placeholderWidth()), 0.5 * cityWidth - 3)
        
        stateTextFieldWidthConstraint.constant = stateWidth
        stateTextFieldWidthConstraint.isActive = true
    }
    
    func placeholderWidth() -> CGFloat {
        let attributedText = NSAttributedString(string: stateTextField.placeholder!, attributes: [NSAttributedString.Key.font: stateTextField.font!])
        let boundsForText = attributedText.boundingRect(with: CGSize.zero, options: .usesLineFragmentOrigin, context: nil)
        let placeholderWidth = ceil(boundsForText.size.width) + 34
        return placeholderWidth
    }
    
    // MARK: - Actions
    
    override func firstTextFieldBecomeFirstResponder() {
        street1TextField.becomeFirstResponder()
    }
    
    @objc func handleContactVCCellShouldResignFirstResponders() {
        
        guard street1TextField != nil else { return }
        
        street1TextField.resignFirstResponder()
        street2TextField.resignFirstResponder()
        cityTextField.resignFirstResponder()
        stateTextField.resignFirstResponder()
        zipTextField.resignFirstResponder()
        countryTextField.resignFirstResponder()
    }
    
    @objc func handleLabelTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCAddrCell.\(#function) ==="))
        
        addrLabels = AddressLabel.labels(context: delegate!.returnContext())
        dataLabelPickerIndexOfSelectedValue = addrLabels.firstIndex(where: {$0 == addr.label})!
        
        showDataLabelPickerVC(values: addrLabels.map({$0.name}), delegate: self)
    }
    
    @objc func handleAddressTap(_ tapGestureRecognizer: UITapGestureRecognizer) {
        Func.ifDebug(print("=== ContactVCEmailCell.\(#function) ==="))
        
        let addrString = addr.singleLineString
        
        guard addrString.count > 0 else { return }
        
        delegate?.presentMapFor(addr)
    }
    
    override func handleTextFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== ContactVCAddrCell.\(#function) ==="))
        
        switch textField{
        case street1TextField:
            addr.street1 = textField.text!
        case street2TextField:
            addr.street2 = textField.text!
        case cityTextField:
            addr.city = textField.text!
        case stateTextField:
            addr.state = textField.text!
        case zipTextField:
            addr.zip = textField.text!
        case countryTextField:
            addr.country = textField.text!
        default:
            break
        }

        checkHasChangesHasChanged()
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCAddrCell {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== ContactVCAddrCell.\(#function) ==="))
        
        if textField == street1TextField {
            street2TextField.becomeFirstResponder()
            
        } else if textField == street2TextField {
            cityTextField.becomeFirstResponder()
            
        } else if textField == cityTextField {
            stateTextField.becomeFirstResponder()
            
        } else if textField == stateTextField {
            zipTextField.becomeFirstResponder()
            
        } else if textField == zipTextField {
            countryTextField.becomeFirstResponder()
            
        } else {
            delegate?.setNextFirstResponder(dataType: dataType, contactData: addr)
        }
        
        return true
    }
}

// MARK: - DataLabelPickerVCDelegate

extension ContactVCAddrCell: DataLabelPickerVCDelegate {
    
    func newIndexOfSelectedValue(_ index: Int) {
        Func.ifDebug(print("=== ContactVCAddrCell.\(#function) === index: \(index), dataLabelPickerIndexOfSelectedValue: \(dataLabelPickerIndexOfSelectedValue!)"))
        
        if index != dataLabelPickerIndexOfSelectedValue {
            addr.label = addrLabels[index]
            typeLabel.text = addr.label!.name
            
            checkHasChangesHasChanged()
        }
    }
}
