//
//  NotificationName.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

struct NotificationName {
    
    static let fetchRequestFailed = Notification.Name(rawValue: "fetchRequestFailedNotification")
    static let contactVCCellShouldResignFirstResponders = Notification.Name(rawValue: "ContactVCCellShouldResignFirstResponders")
}
