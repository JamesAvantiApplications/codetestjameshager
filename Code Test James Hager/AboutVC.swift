//
//  AboutVC.swift
//
//  Created by James Hager on 11/25/15.
//  Copyright © 2015 James Hager. All rights reserved.
//

import UIKit

class AboutVC: SettingsVCGeneral {
    
    override var file: String {
        return "AboutVC"
    }
    
    override func initSettingsDataItems() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        hideAllSeparators = true
        
        var settingsDataItem: SettingsDataItem!
        
        settingsDataItem = SettingsDataItem(text: "This app was developed as a Code Test to audition as an iOS Swift Mobile Developer for Service Fusion.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "It was developed using only the information provided by the customer at the start of the project. If this had been a regular app-development project, there would have been interaction with the customer throughout the project in order to confirm the details of the deliverable.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "One big assumption I made was that this was a test of my ability to develop a typical, dynamic, data-driven app using Core Data and not a test of my ability to use the Contacts framework to interact with a user's regular iOS contacts information.")
        addSettingsDataItem(settingsDataItem)
        
        // Specifications
        
        settingsDataItem = SettingsDataItem(text: "Specifications", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "This Contacts app stores a list of contacts with the following fields:")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "first name,", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "last name,", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "date of birth,", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "zero or more (physical) addresses,", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "one or more phone numbers, and", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "one or more email addresses.", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "It provides basic CRUD functionality and Search.")
        addSettingsDataItem(settingsDataItem)
        
        // Search
        
        settingsDataItem = SettingsDataItem(text: "Search", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
            
        settingsDataItem = SettingsDataItem(text: "The search capability provides complex filtering of the contacts based on the contents of the fields described in the Specifications section.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "The search is case- and diacritic-insensitive, and supports partial, exact (\"\"), \"and\" (+), and \"not\" (-) searching.")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Take the following three contacts as an example.")
        addSettingsDataItem(settingsDataItem)
        
        let nbsp = "\u{00a0}"  // non-breaking space
        
        settingsDataItem = SettingsDataItem(text: "Alice Jones\n(321) 111-1234\nalice.jones@apple.com\n1\(nbsp)Infinite\(nbsp)Loop, Cupertino, CA 95014", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Bob Jackson\n(321) 222-1234\nbob.jackson@servicefusion.com\n8605\(nbsp)Freeport\(nbsp)Parkway, Irving, TX 75063", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Carlos Gómez\n(321) 333-1234\ncarlosg@coolstartup.com", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "The following searches will return:")
        addSettingsDataItem(settingsDataItem)
        
        let separator = " \u{2192} "  // spaces and right arrow
        
        settingsDataItem = SettingsDataItem(text: "j \(separator) Alice & Bob", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "go \(separator) Carlos", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "321 +111 \(separator) Alice", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "321 -111 \(separator) Bob & Carlos", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "\"1 \" \(separator) Alice", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Notes:")
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "The closing quote is not required to get an exact match unless an exact match is followed by an \"and\" (+) or \"not\" (-).", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Exact matches for names can be performed using \"firstName lastName\" or \"lastName, firstName\".", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Searches for a birthday are performed with dates formatted like \"January 1, \(AppInfo.copyrightYear)\".", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        // Bells And Whistles
        
        settingsDataItem = SettingsDataItem(text: "Bells And Whistles", cellType: .sectionCell)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "Labels (home, work, etc) are associated with the data items (address, phone number, and email address) because how useful is a contacts app if you don't know what the contact details are for?", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "The data items have actions (launch Apple Maps, place a call, send an email) because who wants to write down, or commit to memory, the contact details so you can navigate-to, call, or email the person?", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "The data item sort order can be changed in Edit mode by pushing on an item and dragging it to the new position.", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
        
        settingsDataItem = SettingsDataItem(text: "It's a Universal iOS app that operates in master/detail mode on iPads and in single-view mode on all other devices.", cellType: .bullet)
        addSettingsDataItem(settingsDataItem)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        title = "About"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if mdHelper.isMasterDetail {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
}
