//
//  Contact+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension Contact {
    
    @NSManaged var dateOfBirthSave: Date?
    @NSManaged var firstNameSave: String
    @NSManaged var lastNameSave: String
    @NSManaged var searchTextMatch: String
    @NSManaged var searchTextUnique: String
    @NSManaged var sectionKey: String
    @NSManaged var sort: String

    @NSManaged var addresses: Set<Address>?
    @NSManaged var emails: Set<Email>?
    @NSManaged var phones: Set<Phone>?
}
