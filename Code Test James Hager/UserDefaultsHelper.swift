//
//  UserDefaultsHelper.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum UserDefaultsHelper {
    
    static func setUserDefaults() {
        
        let userDefaults = UserDefaults.standard
        setUserDefaultSettings(userDefaults: userDefaults)
        userDefaults.synchronize()
    }
    
    static func setUserDefaultSettings(userDefaults: UserDefaults) {
        
        /*
        // locations
        userDefaults.set(LocationsSettings.regularLocationsSortModeDefault, forKey: LocationsSettings.regularLocationsSortModeKey)
        userDefaults.set(LocationsSettings.showStatusDefault, forKey: LocationsSettings.showStatusKey)
        userDefaults.set(LocationsSettings.showWeatherDefault, forKey: LocationsSettings.showWeatherKey)
        userDefaults.set(LocationsSettings.showSectionIndexDefault, forKey: LocationsSettings.showSectionIndexKey)
        userDefaults.set(LocationsSettings.rememberWeatherTabKeyDefault, forKey: LocationsSettings.rememberWeatherTabKey)
        */
    }
    
    static func createSampleContacts(mdHelper: MasterDetailHelper) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        let context = mdHelper.coreDataStack.context
        
        let addressLabels = AddressLabel.labels(context: context)
        let emailLabels = EmailLabel.labels(context: context)
        let phoneLabels = PhoneLabel.labels(context: context)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let c1 = Contact(context: context)
        c1.firstName = "Alice"
        c1.lastName = "Jones"
        c1.dateOfBirth = dateFormatter.date(from: "2007-01-09")!
        c1.addAddress(Address(street1: "1 Infinite Loop", street2: "", city: "Cupertino", state: "CA", zip: "95014", country: "", label: addressLabels[1], context: context))
        c1.addAddress(Address(street1: "[sample]", street2: "", city: "", state: "", zip: "", country: "", label: addressLabels[2], context: context))
        c1.addEmail(Email(addr: "alice.jones@apple.com", label: emailLabels[1], context: context))
        c1.addPhone(Phone(number: "(321) 111-1234", label: phoneLabels[1], context: context))
        c1.setSearchText()
        
        let c2 = Contact(context: context)
        c2.firstName = "Bob"
        c2.lastName = "Jackson"
        c2.addAddress(Address(street1: "", street2: "", city: "Arlington", state: "TX", zip: "", country: "", label: addressLabels[0], context: context))
        c2.addAddress(Address(street1: "8605 Freeport Parkway #200", street2: "", city: "Irving", state: "TX", zip: "75063", country: "", label: addressLabels[1], context: context))
        c2.addAddress(Address(street1: "[sample]", street2: "", city: "", state: "", zip: "", country: "", label: addressLabels[2], context: context))
        c2.addEmail(Email(addr: "bob-jackson@gmail.com", label: emailLabels[0], context: context))
        c2.addEmail(Email(addr: "bob.jackson@servicefusion.com", label: emailLabels[1], context: context))
        c2.addPhone(Phone(number: "(321) 222-1234", label: phoneLabels[0], context: context))
        c2.addPhone(Phone(number: "(321) 222-4321", label: phoneLabels[1], context: context))
        c2.setSearchText()
        
        let c3 = Contact(context: context)
        c3.firstName = "Carlos"
        c3.lastName = "Gómez"
        c3.addAddress(Address(street1: "[sample]", street2: "", city: "", state: "", zip: "", country: "", label: addressLabels[2], context: context))
        c3.addEmail(Email(addr: "carlosg@coolstartup.com", label: emailLabels[0], context: context))
        c3.addPhone(Phone(number: "(321) 333-1234", label: phoneLabels[0], context: context))
        c3.setSearchText()
        
        #if DEBUG
        c1.describe()
        c2.describe()
        c3.describe()
        #endif
        
        mdHelper.coreDataStack.saveContext()
    }
}
