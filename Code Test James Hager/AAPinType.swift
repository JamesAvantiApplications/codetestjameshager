//
//  AAPinType.swift
//
//  Created by James Hager on 8/25/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Constraint pin types used by UIView+add.swift and UIView+constraints.swift
//

import Foundation

enum AAPinType: String, CustomStringConvertible {
    case superview
    case layoutMargin
    case safeArea
    
    public var description: String { return rawValue }
}
