
import UIKit

class SettingsTextCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var statusColorBar: UIView!
    @IBOutlet weak var statusColorBarL: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIImageView(image: UIImage.imageOfColor(Color.lightLightGreen, size: CGSize(width: 1, height: 1)))
    }
    
    func configureCell(_ text: String, withDisclosureIndicator disclosureIndicator: Bool = false, withStatusColorBarColor statusColorBarColor: UIColor = .clear) {
        label.text = text
        
        if disclosureIndicator {
            accessoryType = .disclosureIndicator
            selectionStyle = .default
        } else {
            accessoryType = .none
            selectionStyle = .none
        }
        
        statusColorBar.backgroundColor = statusColorBarColor
        statusColorBarL.backgroundColor = statusColorBarColor
    }
}
