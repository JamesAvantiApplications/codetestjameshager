//
//  Address+CoreDataProperties.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

extension Address {
    
    @NSManaged var citySave: String
    @NSManaged var countrySave: String
    @NSManaged var stateSave: String
    @NSManaged var street1Save: String
    @NSManaged var street2Save: String
    @NSManaged var zipSave: String
    
    @NSManaged var contact: Contact?
    @NSManaged var label: AddressLabel?
}
