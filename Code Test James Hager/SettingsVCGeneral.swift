//
//  SettingsVCGeneral.swift
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//
//  A base class for settings-like view controllers.
//

import UIKit
import CoreData
import StoreKit
import MessageUI

class SettingsVCGeneral: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    var isMaster = true
    
    var settingsDataItems = [Int: SettingsDataItem]()
    var index = 0
    var settingsDataItemsIndexForIAP = [Int: Int]()
    var cellsToRegister: Set<SettingsCellIdentifiers> = []
    
    var defaultCellSeparatorInset: UIEdgeInsets!
    let noCellSeparatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: .greatestFiniteMagnitude)
    var hideAllSeparators = false
    
    var doAdjustFonts = false
    
    var navSeparator: UIView!
    var tableView: UITableView!
    
    var file: String {
        return "SettingsViewControllerGeneral"
    }
    
    func initSettingsDataItems() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
    }
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        initSettingsDataItems()
        
        let views = Func.makeNavSeparatorAndTableView(inView: view, dataSource: self, delegate: self)
        navSeparator = views.navSeparator
        tableView = views.tableView
        
        registerCells()
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.separatorColor = Color.separator
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Func.ifDebug(print("-----------------------------------------"))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("-----------------------------------------"))
        
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let defaultCellSeparatorInset = defaultCellSeparatorInset else { return }
        
        if tableView.separatorInset.left != defaultCellSeparatorInset.left {
            self.defaultCellSeparatorInset = tableView.separatorInset
            tableView.reloadData()
        }
    }
    
    // MARK: - Setup
    
    func registerCells() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        for settingsCellIdentifier in cellsToRegister {
            Func.ifDebug(print("--- \(file).\(#function) - '\(settingsCellIdentifier)'"))
            tableView.register(UINib(nibName: settingsCellIdentifier.rawValue, bundle: nil), forCellReuseIdentifier: settingsCellIdentifier.rawValue)
        }
    }
    
    func addSettingsDataItem(_ settingsDataItem: SettingsDataItem) {
        Func.ifDebug(print("=== \(file).\(#function) === type=\(settingsDataItem.dataType) === \(Func.debugDate())"))
        settingsDataItems[index] = settingsDataItem
        
        var previousSettingsDataItemsIndexForIAPValue: Int!
        if let previousSettingsDataItemsIndexForIAP = settingsDataItemsIndexForIAP[index - 1] {
            previousSettingsDataItemsIndexForIAPValue = previousSettingsDataItemsIndexForIAP
        } else {
            previousSettingsDataItemsIndexForIAPValue = -1
        }
        
        if settingsDataItem.dataType == .upgradeToFullVersion {
            settingsDataItemsIndexForIAP[index] = previousSettingsDataItemsIndexForIAPValue + 2
        } else {
            settingsDataItemsIndexForIAP[index] = previousSettingsDataItemsIndexForIAPValue + 1
        }
        
        cellsToRegister.insert(settingsDataItem.cellType)
        
        index += 1
    }
    
    func rowToShowForIndexPath(_ indexPath: IndexPath) -> Int {
        let rowToShow = indexPath.row
        Func.ifDebug(print("=== \(file).\(#function) \(Func.displayPath(indexPath)) -> \(rowToShow) === \(Func.debugDate())"))
        return rowToShow
    }
    
    func setBottomSeperator(indexPath: IndexPath, cell: UITableViewCell) {
        
        if hideAllSeparators {
            cell.separatorInset = noCellSeparatorInset
            return
        }
        
        var nextRowIndexPath = indexPath
        nextRowIndexPath.row += 1
        
        if defaultCellSeparatorInset == nil {
            defaultCellSeparatorInset = cell.separatorInset
        }
        
        var hideBottomSeperator = false
        
        if nextRowIndexPath.row < tableView(tableView, numberOfRowsInSection: indexPath.section) {
            let rowToShow = rowToShowForIndexPath(nextRowIndexPath)
            let settingsDataItem = settingsDataItems[rowToShow]!
            if settingsDataItem.cellType == .sectionCell {
                hideBottomSeperator =  true
            }
        }
        
        if hideBottomSeperator {
            cell.separatorInset = noCellSeparatorInset
        } else {
            cell.separatorInset = defaultCellSeparatorInset
        }
    }
}

// MARK: - UITableViewDataSource

extension SettingsVCGeneral: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        Func.ifDebug(print("=== \(file).\(#function) -> 1 ==="))
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = settingsDataItems.count
        Func.ifDebug(print("=== \(file).\(#function) -> \(numberOfRows) === \(Func.debugDate())"))
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        let rowToShow = rowToShowForIndexPath(indexPath)
        let settingsDataItem = settingsDataItems[rowToShow]!
        
        #if DEBUG
        if let text = settingsDataItem.text {
            print("--- \(file).\(#function) - text: \(text)")
        }
        #endif
        
        switch settingsDataItem.cellType {
        case .textCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsTextCell
            if settingsDataItem.color == nil {
                settingsDataItem.color = .clear
            }
            cell.configureCell(settingsDataItem.text, withDisclosureIndicator: settingsDataItem.disclosureIndicator, withStatusColorBarColor: settingsDataItem.color)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .segmentedControlCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSegmentedControlCell
            var enabled: Bool!
            if let closure = settingsDataItem.enabledOnClosure {
                enabled = closure()
            } else {
                enabled = true
            }
            cell.configureCell(settingsDataItem.text, segmentedControlName: settingsDataItem.segmentedControlName, values: settingsDataItem.values as! [String], segmentWidth: settingsDataItem.segmentWidth, enabled: enabled)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .switchCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSwitchCell
            var enabled: Bool!
            if let closure = settingsDataItem.enabledOnClosure {
                enabled = closure()
            } else {
                enabled = true
            }
            cell.configureCell(settingsDataItem.text, switchName: settingsDataItem.switchName, enabled: enabled)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .valueCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsValueCell
            cell.configureCell(settingsDataItem.text, valueName: settingsDataItem.valueName, valueType: settingsDataItem.valueType, values: settingsDataItem.values)
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .sectionCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsSectionCell
            cell.configureCell(settingsDataItem.text, textColor: settingsDataItem.color)
            cell.separatorInset = noCellSeparatorInset
            return cell
            
        case .textSmallCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsTextSmallCell
            
            if settingsDataItem.dataType == .version {
                var text = AppInfo.appNameWithFullVersion
                text += "\n© \(AppInfo.copyrightYear) Avanti Applications, LLC"
                cell.configureCell(text)
                
            } else if let closure = settingsDataItem.textFromClosure {
                cell.configureCell(closure())
                
            } else {
                cell.configureCell(settingsDataItem.text)
            }
            
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            cell.selectionStyle = .none
            return cell
            
        // AboutVC
            
        case .bullet:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsBulletItemCell
            cell.configureCell(settingsDataItem.text)
            cell.selectionStyle = .none
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsImageTextCell
            cell.configureCell(settingsDataItem.imageName, tintColor: settingsDataItem.color, text: settingsDataItem.text)
            cell.selectionStyle = .none
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .imageSmall:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsImageTextCell
            cell.configureCell(settingsDataItem.imageName, text: settingsDataItem.text)
            cell.selectionStyle = .none
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
            
        case .imageSmallWithHeader:
            let cell = tableView.dequeueReusableCell(withIdentifier: settingsDataItem.cellType.rawValue, for: indexPath) as! SettingsImageTextCell
            cell.configureCell(settingsDataItem.imageName, text: settingsDataItem.text)
            cell.selectionStyle = .none
            setBottomSeperator(indexPath: indexPath, cell: cell as UITableViewCell)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate

extension SettingsVCGeneral: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        let rowToShow = rowToShowForIndexPath(indexPath)
        let settingsDataItem = settingsDataItems[rowToShow]!
        
        if let closure = settingsDataItem.closure {
            closure()
            
        } else if settingsDataItem.dataType == .valuePicker {
            let vc = SettingsValueVC()
            vc.mdHelper = mdHelper
            if mdHelper.isMasterDetail {
                vc.delegate = self
                vc.indexPathInSettingsVC = indexPath
            }
            vc.viewTitle = settingsDataItem.text
            vc.valueName = settingsDataItem.valueName
            vc.valueType = settingsDataItem.valueType
            vc.values = settingsDataItem.values
            
            mdHelper.presentViewControllerOnDetail(vc)
        }
    }
}


// MARK: - UITableViewDelegate Methods

extension SettingsVCGeneral: SettingsValueViewControllerDelegate {
    
    func reloadSettingsRow(at indexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}
