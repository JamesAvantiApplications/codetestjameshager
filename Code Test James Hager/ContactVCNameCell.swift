//
//  ContactVCNameCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  ContactVC Name Cell - only used for dispMode != .disp
//

import UIKit

class ContactVCNameCell: ContactVCCell {
    
    var contact: Contact!
    
    var firstNameTextField: UITextField!
    var lastNameTextField: UITextField!
    
    var initialFirstName = ""
    var initialLastName = ""
    
    override var hasChanges: Bool {
        return firstNameTextField.text != initialFirstName
            || lastNameTextField.text != initialLastName
    }
    
    override var idForChange: NSObject {
        return dataType.rawValue as NSString
    }
    
    // MARK: - Configure
    
    func configure(contact: Contact, dispMode: ContactVCDispMode, delegate: ContactVCCellDelegate) {
        dataType = .name
        
        self.contact = contact
        configure(delegate: delegate)
        
        initialFirstName = contact.firstName
        initialLastName = contact.lastName
        
        if firstNameTextField == nil {
            setUpView()
        }
        
        setView(dispMode: dispMode)
        
        firstNameTextField.text = contact.firstName
        lastNameTextField.text = contact.lastName
    }
    
    override func setView(dispMode: ContactVCDispMode) {
        setTextField(firstNameTextField, forDispMode: dispMode)
        setTextField(lastNameTextField, forDispMode: dispMode)
    }
    
    // MARK: - Setup View
    
    override func setUpView() {
        
        firstNameTextField = makeTextField(placeholder: "First name")
        firstNameTextField.add(toView: contentView, pinTypes: [.layoutMargin, .layoutMargin, nil, .layoutMargin])
        firstNameTextField.autocapitalizationType = .words
        
        lastNameTextField = makeTextField(placeholder: "Last name")
        lastNameTextField.add(toView: contentView, pinTypes: [nil, .layoutMargin, .layoutMargin, .layoutMargin])
        lastNameTextField.pinTop(toBottomOf: firstNameTextField, margin: 2)
        lastNameTextField.autocapitalizationType = .words
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleContactVCCellShouldResignFirstResponders),
            name: NotificationName.contactVCCellShouldResignFirstResponders,
            object: nil)
    }
    
    // MARK: - Actions
    
    override func firstTextFieldBecomeFirstResponder() {
        Func.ifDebug(print("=== ContactVCNameCell.\(#function) ==="))
        firstNameTextField.becomeFirstResponder()
    }
    
    @objc func handleContactVCCellShouldResignFirstResponders() {
        Func.ifDebug(print("=== ContactVCNameCell.\(#function) ==="))
        firstNameTextField.resignFirstResponder()
        lastNameTextField.resignFirstResponder()
    }
    
    override func handleTextFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== ContactVCNameCell.\(#function) ==="))
        
        switch textField{
        case firstNameTextField:
            contact.firstName = textField.text!
        case lastNameTextField:
            contact.lastName = textField.text!
        default:
            break
        }
        
        checkHasChangesHasChanged()
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCNameCell {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        if textField == firstNameTextField {
            lastNameTextField.becomeFirstResponder()
            
        } else {
            delegate?.setNextFirstResponder(dataType: dataType, contactData: nil)
        }
        
        return true
    }
}
