//
//  AppInfo.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/27/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import Foundation

enum AppInfo {
    
    static var appName: String {
        return "Code Test James Hager"
    }
    
    static var appNameWithFullVersion: String {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let bundle = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        return "\(appName) \(version) (\(bundle))"
    }
    
    static var appNameWithVersion: String {
        let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        return "\(appName) \(version)"
    }
    
    static var copyrightYear: String {
        return "2019"
    }
}
