//
//  ContactsVC.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import CoreData

protocol ContactsVCDelegate {
    func alertForAddContact() -> UIAlertController?
    func alertForWillSelectContact(_ contact: Contact) -> UIAlertController?
}

// MARK: -

class ContactsVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    var delegate: ContactsVCDelegate?
    
    var fetchRequest: NSFetchRequest<NSFetchRequestResult>!
    var fetchedResultsController: AAFetchedResultsController!
    
    let tableViewCellIdentifier = "ContactsVCCell"
    
    var searchText = ""
    
    var reloadInViewWillAppear = false
    
    var keyboardHelper: KeyboardHelper!
    
    var searchBarContainerView: UIView!
    var searchBar: UISearchBar!
    var tableView: UITableView!
    
    var searchBarBotMarginConstraint: NSLayoutConstraint!
    
    var searchBarTextField: UITextField!
    
    var context: NSManagedObjectContext {
        return mdHelper.coreDataStack.context
    }
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        title = "Contacts"
        
        setUpView()
        
        keyboardHelper = KeyboardHelper(view: view)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: tableViewCellIdentifier)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        setUpNavigationItems()
        
        if mdHelper.isMasterDetail {
            // make sure that a ContactVC is presenting the selected Contact

            if let indexPath = self.tableView.indexPathForSelectedRow {
                if let _ = mdHelper.detailNavController.topViewController as? ContactVC  {
                } else {
                    let contact = fetchedResultsController.object(at: indexPath) as! Contact
                    mdHelper.presentContact(contact: contact)
                }
            }
        }
        
        /*
        if reloadInViewWillAppear {
        }
        */
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Func.ifDebug(print("=== \(file).\(#function) === view size: (W,H)=\(view.frame.size), separatorInset: \(tableView.separatorInset.left) === \(Func.debugDate())"))
        
        setSearchBarBotMarginConstraint()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        tableView.flashScrollIndicators()
    }
    
    // MARK: - Setup Methods
    
    func setUpView() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        view.backgroundColor = .white
        
        // Set up the search bar so it looks like it's in the nav bar
        
        searchBarContainerView = UIView(frame: CGRect.zero)
        searchBarContainerView.add(toView: view, pinTypes: [.superview, .superview, nil, .superview], margins: [-9, nil, nil, nil])
        
        searchBarContainerView.backgroundColor = .white
        
        searchBar = UISearchBar(frame: CGRect.zero)
        searchBar.add(toView: searchBarContainerView, pinTypes: [.superview, .safeArea, nil, .safeArea])
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.barTintColor = .white  // this is the color outside the searchField
        searchBar.tintColor = Color.mediumGreen  // this is the color of the Cancel button
        
        if let textField = searchBar.value(forKey: "searchField") as? UITextField {
            searchBarTextField = textField
            searchBarTextField.layer.cornerRadius = 9
            searchBarTextField.layer.borderWidth = 1
            
            setSearchBarTextField(forEditing: false, animate: false)
        }
        
        searchBar.autocapitalizationType = .none
        searchBar.autocorrectionType = .no
        searchBar.smartQuotesType = .no
        searchBar.smartDashesType = .no
        
        // botMargin covers the usual border of the UISearchBar and provides extra space below the searchBar
        
        let botMargin = UIView(frame: CGRect.zero)
        botMargin.add(toView: searchBarContainerView, pinTypes: [nil, .superview, .superview, .superview])
        searchBarBotMarginConstraint = botMargin.heightAnchor.constraint(equalToConstant: 6)
        searchBarBotMarginConstraint.isActive = true
        botMargin.pinTop(toBottomOf: searchBar, margin: -1)
        botMargin.backgroundColor = .white
        
        let navSeparator = Func.makeNavSeparator()
        navSeparator.add(toView: view, pinTypes: [nil, .superview, nil, .superview])
        navSeparator.pinTop(toBottomOf: searchBarContainerView)
        
        tableView = UITableView(frame: CGRect.zero)
        tableView.add(toView: view, pinTypes: [nil, .superview, .superview, .superview])
        tableView.pinTop(toBottomOf: navSeparator)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.sectionIndexColor = Color.avantiGreen
        tableView.sectionIndexBackgroundColor = .clear
        tableView.separatorColor = Color.separator
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func setUpNavigationItems() {
        
        // left bar button
        
        let image = UIImage(named: "Settings")!.imageWithColor(Color.mediumGreen)!
        let menuButton = UIButton(frame: CGRect(origin: CGPoint.zero, size: image.size))
        menuButton.setBackgroundImage(image, for: UIControl.State())
        menuButton.addTarget(self, action: #selector(showSettings), for: .touchUpInside)
        let settingsBarButton = UIBarButtonItem(customView: menuButton)
        
        //navigationItem.leftBarButtonItem = settingsBarButton
        
        let spaceBarButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spaceBarButton.width = 8
        
        navigationItem.setLeftBarButtonItems([settingsBarButton,spaceBarButton], animated: false)
        
        // right bar button
        
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addContact))
        
        navigationItem.rightBarButtonItem = addBarButton
    }
    
    func setSearchBarBotMarginConstraint() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        guard let navHeight = navigationController?.navigationBar.frame.height else { return }
        
        // settings icon 22
        // searchBar built-in padding
        
        let desiredSearchBarBotMarginConstraintConstant = 0.5 * (navHeight - 22) - 7
        
        if desiredSearchBarBotMarginConstraintConstant != searchBarBotMarginConstraint.constant {
            searchBarBotMarginConstraint.isActive = false
            searchBarBotMarginConstraint.constant = desiredSearchBarBotMarginConstraintConstant
            searchBarBotMarginConstraint.isActive = true
            searchBarContainerView.layoutIfNeeded()
        }
    }
    
    func setSearchBarTextField(forEditing: Bool, animate: Bool) {
        
        if searchBarTextField == nil {
            return
        }
        
        var backgroundColor: UIColor!
        var borderColor: CGColor!
        
        if forEditing {
            backgroundColor = Color.lightLightGreen
            borderColor = Color.separator.cgColor
        } else {
            backgroundColor = Color.lightLightGray
            borderColor = Color.lightGreen.cgColor
        }
        
        if animate {
            let caAnimation = CABasicAnimation(keyPath: "borderColor")
            caAnimation.fromValue = searchBarTextField.layer.borderColor
            caAnimation.toValue = borderColor
            caAnimation.duration = 0.2
            searchBarTextField.layer.add(caAnimation, forKey: "borderColor")
            searchBarTextField.layer.borderColor = borderColor

            UIView.animate(withDuration: 0.2) {
                self.searchBarTextField.backgroundColor = backgroundColor
            }
            
        } else {
            searchBarTextField.backgroundColor = backgroundColor
            searchBarTextField.layer.borderColor = borderColor
        }
    }
    
    // MARK: - Navigation
    
    @objc func showSettings() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        reloadInViewWillAppear = true
        
        let settingsVC = SettingsVC()
        settingsVC.mdHelper = mdHelper
        
        searchBar.resignFirstResponder()
        
        mdHelper.presentViewControllerOnMasterAndBlankOnDetail(vc: settingsVC)
    }
    
    @objc func addContact() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if let alert = delegate?.alertForAddContact() {
            present(alert, animated: true, completion: nil)
        } else {
            doAddContact()
        }
    }
    
    func doAddContact() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let contact = Contact(context: context)
        contact.setUpNew(context: context)
        
        if mdHelper.isMasterDetail {
            // select the new Contact
            Func.afterDelay(0.1) {
                if let indexPath = self.fetchedResultsController.indexPath(forObject: contact) {
                    self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .top)
                }
            }
        }
        
        mdHelper.presentContact(contact: contact, dispMode: .add)
    }
    
    // MARK: - Fetch Methods
    
    func defineFetchedResultsController() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        fetchRequest = NSFetchRequest()
        
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: EntityName.contact.rawValue, in: context)
        
        let sectionSortDescriptor = NSSortDescriptor(key: "sectionKey", ascending: true)
        let secondSortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        fetchRequest.sortDescriptors = [sectionSortDescriptor, secondSortDescriptor]
        
        fetchRequest.fetchBatchSize = 20
        
        fetchedResultsController = AAFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: context,
            sectionNameKeyPath: "sectionKey",
            cacheName: nil)
        
        fetchedResultsController.delegate = self
    }
    
    func performFetch() {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        //Func.ifDebug(print("--- \(file).\(#function) - predicate = \(String(describing: fetchRequest.predicate))"))
        
        do {
            try fetchedResultsController.performFetch()
            
        } catch {
            NotificationCenter.default.post(name: NotificationName.fetchRequestFailed, object: nil)
        }
    }
    
    func performFetchAndReloadData() {
        performFetch()
        tableView.reloadData()
    }
    
    func doFirstFetch() {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        
        defineFetchedResultsController()
        performFetch()
    }
    
    // MARK: - Keyboard
    
    @objc func keyboardNotification(_ notification: Notification) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        guard let contentInset = keyboardHelper.contentInset(forTableView: tableView, forNotification: notification) else { return }
        
        UIView.animate(withDuration: keyboardHelper.duration, delay: 0, options: keyboardHelper.animationOptions, animations: {
            self.tableView.contentInset = contentInset
        })
    }
    
    // MARK: - Misc Methods
    
    func handleSearchText(_ searchText: String) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("--- - searchText: '\(searchText)'"))
        
        if searchText == self.searchText {
            return
        }
        
        self.searchText = searchText
        
        if searchText == "" {
            fetchRequest.predicate = nil
            
        } else {
            fetchRequest.predicate = SearchStringHelper().predicateFromSearchString(searchText)
        }
        
        performFetchAndReloadData()
    }
}

// MARK: - UITableViewDataSource

extension ContactsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        #if DEBUG
        if fetchedResultsController == nil {
            print("=== \(file).\(#function) - 0")
        } else {
            print("=== \(file).\(#function) - \(fetchedResultsController.sections!.count)")
        }
        #endif
        
        if fetchedResultsController == nil {
            return 0
        } else {
            return fetchedResultsController.sections!.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        Func.ifDebug(print("=== \(file).\(#function) === section: \(section) -> \(sectionInfo.numberOfObjects)"))
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        let contact = fetchedResultsController.object(at: indexPath) as! Contact
        
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier)!
        cell.textLabel?.text = contact.nameLastFirst
        /*
        if !mdHelper.isMasterDetail {
            cell.selectionStyle = .none
        }
        */
        cell.selectedBackgroundView = UIImageView(image: UIImage.imageOfColor(Color.lightGreen, size: CGSize(width: 1, height: 1)))
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        let contact = fetchedResultsController.object(at: indexPath) as! Contact
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            if self.mdHelper.isMasterDetail {
                // rollBackDetailToNoContact if contact == contactPresented
                if let contactPresented = self.mdHelper.contactPresented {
                    if contact == contactPresented {
                        self.mdHelper.rollBackDetailToNoContact()
                    }
                }
            }
            
            let contact = self.fetchedResultsController.object(at: indexPath) as! Contact
            contact.deleteSelf(context: self.context)
            
            self.mdHelper.coreDataStack.saveContext()
            
            self.performFetchAndReloadData()
        }
        
        return [delete]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return fetchedResultsController.sectionIndexTitles
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchedResultsController.sectionIndexTitles[section]
    }
}

// MARK: - UITableViewDelegate

extension ContactsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = Color.lightLightGreen
            headerView.textLabel?.textColor = .black
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        if !mdHelper.isMasterDetail {
            // always allow slection for not master/detail
            return indexPath
        }
        
        // see if selecting the selected row
        
        if let indexPathForSelectedRow = self.tableView.indexPathForSelectedRow {
            if indexPath == indexPathForSelectedRow {
                return nil
            }
        }
        
        // see if add/editing in ContactVC
        
        guard let delegate = delegate else { return indexPath }
        
        let contact = fetchedResultsController.object(at: indexPath) as! Contact
        
        guard let alert = delegate.alertForWillSelectContact(contact) else { return indexPath }
        
        present(alert, animated: true, completion: nil)
        
        return nil
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = fetchedResultsController.object(at: indexPath) as! Contact
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) - name: \(contact.nameLastFirst)"))
        
        if !mdHelper.isMasterDetail {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        mdHelper.presentContact(contact: contact, dispMode: .disp)
    }
}

// MARK: - NSFetchedResultsControllerDelegate

extension ContactsVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        Func.ifDebug(print("*** \(file).\(#function), \(Func.debugDate())"))
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeInsert (object), \(Func.debugDate()), indexPath: \(Func.displayPath(indexPath)), newIndexPath: \(Func.displayPath(newIndexPath))"))
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            
        case .delete:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeDelete (object), \(Func.debugDate()), indexPath: \(Func.displayPath(indexPath)), newIndexPath: \(Func.displayPath(newIndexPath))"))
            tableView.deleteRows(at: [indexPath!], with: .fade)
            
        case .update:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeUpdate (object), \(Func.debugDate()), indexPath: \(Func.displayPath(indexPath)), newIndexPath: \(Func.displayPath(newIndexPath))"))
            
            if let indexPath = indexPath,
                let cell = tableView.cellForRow(at: indexPath) {
                let contact = anObject as! Contact
                cell.textLabel?.text = contact.nameLastFirst
            }
            
        case .move:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeMove (object), \(Func.debugDate()), indexPath: \(Func.displayPath(indexPath)), newIndexPath: \(Func.displayPath(newIndexPath))"))
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
            
            if mdHelper.isMasterDetail {
                // adjust the selection
                if let indexPathSelected = tableView.indexPathForSelectedRow {
                    if indexPath! == indexPathSelected {
                        Func.afterDelay(0.1) {
                            self.tableView.selectRow(at: newIndexPath!, animated: false, scrollPosition: .top)
                        }
                    }
                }
            }
            
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeInsert (section), \(Func.debugDate()) - sectionIndex: \(sectionIndex)"))
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
            
        case .delete:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeDelete (section), \(Func.debugDate()) - sectionIndex: \(sectionIndex)"))
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
            
        case .update:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeUpdate (section), \(Func.debugDate()) - sectionIndex: \(sectionIndex)"))
            
        case .move:
            Func.ifDebug(print("*** \(file) NSFetchedResultsChangeMove (section), \(Func.debugDate()) - sectionIndex: \(sectionIndex)"))
        }
        
        tableView.reloadSectionIndexTitles()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        Func.ifDebug(print("*** \(file).\(#function), \(Func.debugDate())"))
        tableView.endUpdates()
    }
}

// MARK: - UISearchBarDelegate

extension ContactsVC: UISearchBarDelegate {
    
    // func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool)
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        setSearchBarTextField(forEditing: true, animate: true)
        
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        
        setSearchBarTextField(forEditing: false, animate: true)
        
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchText = searchBar.text!
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("--- - searchText: '\(searchText)'"))
        
        handleSearchText(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("--- - searchText: '\(searchText)'"))
        
        searchBar.text = ""
        handleSearchText("")
        
        searchBar.setShowsCancelButton(false, animated: true)
        
        searchBar.resignFirstResponder()
    }
}

// MARK: - ContactVCDelegate

extension ContactsVC: ContactVCDelegate {
    
    func addAndPresentContact() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        doAddContact()
    }
    
    func selectAndPresentContact(_ contact: Contact) {
        Func.ifDebug(print("=== \(file).\(#function) === name: '\(contact.nameNatural)' === \(Func.debugDate())"))
        
        let indexPath = fetchedResultsController.indexPath(forObject: contact)
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        mdHelper.presentContact(contact: contact, dispMode: .disp)
    }
}
