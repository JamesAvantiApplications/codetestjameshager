//
//  SettingsCellIdentifiers.swift
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

enum SettingsCellIdentifiers: String {
    case sectionCell = "SettingsSectionCell"
    case textCell = "SettingsTextCell"
    case textSmallCell = "SettingsTextSmallCell"
    case segmentedControlCell = "SettingsSegmentedControlCell"
    case switchCell = "SettingsSwitchCell"
    case valueCell = "SettingsValueCell"
    
    // AboutVC cells
    case bullet = "SettingsBulletItemCell"
    case image = "SettingsImageTextCell"
    case imageSmall = "SettingsImageSmallTextCell"
    case imageSmallWithHeader = "SettingsImageSmallWithHeaderTextCell"
}
