//
//  ContactVC.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/17/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

import UIKit
import CoreData
import MessageUI
import CoreLocation
import MapKit

protocol ContactVCDelegate {
    func addAndPresentContact()
    func selectAndPresentContact(_ contact: Contact)
}

// MARK: -

class ContactVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    var contact: Contact!
    
    var delegate: ContactVCDelegate?
    
    var contactToSelect: Contact?
    
    var dispMode: ContactVCDispMode = .disp
    
    var idsWithChanges = Set<NSObject>() {
        didSet {
            doneBarButton?.isEnabled = idsWithChanges.count > 0
        }
    }
    
    var nameOnEntry = ""
    
    var phones = [Phone]()
    var emails = [Email]()
    var addresses = [Address]()
    
    var sectionTypes = [DataType]()
    
    struct TableViewCellIdentifier {
        static let add = "ContactVCAddCell"
        static let addr = "ContactVCAddrCell"
        static let dob = "ContactVCDoBCell"
        static let email = "ContactVCEmailCell"
        static let name = "ContactVCNameCell"
        static let phone = "ContactVCPhoneCell"
    }
    
    var reloadInViewWillAppear = true
    
    var geocoder: CLGeocoder!
    var doGeocode = true
    var geocodeSearchingAlert: UIAlertController!
    
    var backBarButton: UIBarButtonItem!
    var doneBarButton: UIBarButtonItem!
    
    var navSeparator: UIView!
    var nameLabel: UILabel!
    var tableView: UITableView!
    
    var constraintsForDisp = [NSLayoutConstraint]()
    var constraintsForAddEdit = [NSLayoutConstraint]()
    
    var sizeLayedOut: CGSize!
    
    var viewSizeLayedOutEdit = CGSize.zero
    var fontSizeLayedOutEdit: CGFloat!
    
    var keyboardHelper: KeyboardHelper!
    
    var context: NSManagedObjectContext {
        return mdHelper.coreDataStack.context
    }
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        keyboardHelper = KeyboardHelper(view: view)
        
        setUpView()
        
        registerCells()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if reloadInViewWillAppear {
            updateDisplay()
            reloadInViewWillAppear = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeLayedOut = view.frame.size
        Func.ifDebug(print("=== \(file).\(#function) === view size: (W,H)=\(view.frame.size), separatorInset: \(tableView.separatorInset.left) === \(Func.debugDate())"))
        
        if dispMode != .disp {
            handleCellConstraintChanges()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        tableView?.flashScrollIndicators()
        
        if dispMode == .add {
            if mdHelper.isMasterDetail {
                // delay setFirstResponderForAdd() to ensure that the tableView has been refreshed and indexPath[0,0] exists
                Func.afterDelay(0.1) {
                    self.setFirstResponderForAdd()
                }
            } else {
                setFirstResponderForAdd()
            }
        }
    }
    
    // MARK: - Setup Views
    
    func setUpView() {
        Func.ifDebug(print("=== \(file).\(#function) === dispMode: \(dispMode) === \(Func.debugDate())"))
        
        view.backgroundColor = .white
        
        nameLabel = makeLabel(fontTextStyle: .headline)
        nameLabel.add(toView: view, pinTypes: [nil, .layoutMargin, nil, .layoutMargin])
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = .white
        
        navSeparator = Func.makeNavSeparator()
        navSeparator.add(toView: view, pinTypes: [nil, .superview, nil, .superview])
        navSeparator.pinTop(toBottomOf: nameLabel, margin: 8)
        //navSeparator.pinTop(toBottomOf: nameLabel)
        
        tableView = UITableView(frame: CGRect.zero)
        tableView.add(toView: view, pinTypes: [nil, .superview, .superview, .superview])
        tableView.pinTop(toBottomOf: navSeparator, margin: -0.5)  // margin is so that the tableView section headers go under the navSeparator
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureRecognized(_:)))
        tableView.addGestureRecognizer(longPressRecognizer)
        
        view.bringSubviewToFront(navSeparator)  // put the navSeparator on top of the tableView
        
        let nameLabelTopConstraint = nameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: -3)
        
        let navSeparatorTopConstraint = navSeparator.topAnchor.constraint(equalTo: view.topAnchor, constant: 1)
        
        constraintsForDisp = [nameLabelTopConstraint]
        constraintsForAddEdit = [navSeparatorTopConstraint]
        
        setConstraintsForDispMode()
    }
    
    func setConstraintsForDispMode() {
        Func.ifDebug(print("=== \(file).\(#function) === dispMode: \(dispMode) === \(Func.debugDate())"))
        
        NSLayoutConstraint.deactivate(constraintsForDisp)
        NSLayoutConstraint.deactivate(constraintsForAddEdit)
        
        if dispMode == .disp {
            NSLayoutConstraint.activate(constraintsForDisp)
        } else {
            NSLayoutConstraint.activate(constraintsForAddEdit)
        }
    }
    
    func setNavigationForDispMode() {
        Func.ifDebug(print("=== \(file).\(#function) === dispMode: \(dispMode) === \(Func.debugDate())"))
        
        navigationItem.title = dispMode.navTitle
        
        if dispMode == .disp {
            if mdHelper.isMasterDetail {
                navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            } else {
                navigationItem.leftBarButtonItem = nil
            }
            
            let editBarButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(edit))
            navigationItem.setRightBarButtonItems([editBarButton], animated: false)
            
        } else {
            let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
            navigationItem.leftBarButtonItem = cancelBarButton
            
            doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
            doneBarButton.isEnabled = idsWithChanges.count > 0
            
            if dispMode == .add {
                navigationItem.setRightBarButtonItems([doneBarButton], animated: false)
            } else {
                let deleteBarButton = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deleteContact))
                navigationItem.setRightBarButtonItems([doneBarButton, deleteBarButton], animated: false)
            }
        }
    }
    
    func makeLabel(fontTextStyle: UIFont.TextStyle) -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.preferredFont(forTextStyle: fontTextStyle)
        label.adjustsFontForContentSizeCategory = true
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }
    
    func registerCells() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        tableView.register(ContactVCAddCell.self, forCellReuseIdentifier: TableViewCellIdentifier.add)
        tableView.register(ContactVCAddrCell.self, forCellReuseIdentifier: TableViewCellIdentifier.addr)
        tableView.register(ContactVCDoBCell.self, forCellReuseIdentifier: TableViewCellIdentifier.dob)
        tableView.register(ContactVCEmailCell.self, forCellReuseIdentifier: TableViewCellIdentifier.email)
        tableView.register(ContactVCNameCell.self, forCellReuseIdentifier: TableViewCellIdentifier.name)
        tableView.register(ContactVCPhoneCell.self, forCellReuseIdentifier: TableViewCellIdentifier.phone)
    }
    
    func handleCellConstraintChanges() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        var doConstraintChange = view.frame.size != viewSizeLayedOutEdit
        
        viewSizeLayedOutEdit = view.frame.size
        
        let bodyFont = UIFont.preferredFont(forTextStyle: .body)
        
        if fontSizeLayedOutEdit == nil {
            fontSizeLayedOutEdit = bodyFont.pointSize
            
        } else if bodyFont.pointSize != fontSizeLayedOutEdit {
            fontSizeLayedOutEdit = bodyFont.pointSize
            doConstraintChange = true
        }
        
        if !doConstraintChange {
            return
        }
            
        let section = 3
        for row in 0..<tableView.numberOfRows(inSection: section) - 1 {
            Func.ifDebug(print("--- \(file).\(#function) row: \(row)"))
            if let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) as? ContactVCAddrCell {
                cell.updateConstraintsForNewSizes()
            }
        }
    }
    
    func doTransition() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        guard let navView = navigationController?.view else { return }
        
        let snapshot = NavigationHelper.snapshotView(navView)
        
        UIView.animate(withDuration: NavigationHelper.fadeDuration, animations: {
            snapshot.alpha = 0
        }, completion: { _ in
            snapshot.removeFromSuperview()
        })
    }
    
    // MARK: - Setup Contact
    
    func setUpContact(_ contact: Contact, dispMode: ContactVCDispMode? = nil) {
        // call with dispMode = nil to keep the current self.dispMode
        #if DEBUG
        if dispMode == nil {
            print("=== \(file).\(#function) === dispMode: nil === \(Func.debugDate())")
        } else {
            print("=== \(file).\(#function) === dispMode: \(dispMode!) === \(Func.debugDate())")
        }
        #endif
        
        if self.contact != nil && contact != self.contact! {
            idsWithChanges = Set<NSObject>()
        }
        
        self.contact = contact
        
        if let dispMode = dispMode {
            self.dispMode = dispMode
        }
        
        setUpContactData()
        
        if nameLabel != nil {
            if mdHelper.isMasterDetail {
                doTransition()
            }
            
            updateDisplay()
            
            if mdHelper.isMasterDetail && dispMode == .add {
                setFirstResponderForAdd()
            }
        }
    }
    
    func setUpContactData() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if let phones = contact.phones {
            self.phones = Array(phones)
            self.phones.sort(by: { $0.sort < $1.sort })
        } else {
            self.phones = [Phone]()
        }
        
        if let emails = contact.emails {
            self.emails = Array(emails)
            self.emails.sort(by: { $0.sort < $1.sort })
        } else {
            self.emails = [Email]()
        }
        
        if let addresses = contact.addresses {
            self.addresses = Array(addresses)
            self.addresses.sort(by: { $0.sort < $1.sort })
        } else {
            self.addresses = [Address]()
        }
    }
    
    func clearContactData() {
        self.phones = [Phone]()
        self.emails = [Email]()
        self.addresses = [Address]()
    }
    
    func checkData() {
        // make sure that each ContactData item hasData, but let one email and one phone be blank
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        for index in (0..<addresses.count).reversed() {
            Func.ifDebug(print("--- \(file).\(#function) - addresses - index: \(index)"))
            if !addresses[index].hasData {
                contact.deleteAddress(addresses[index], context: context)
                addresses.remove(at: index)
            }
        }
        
        if emails.count == 0 {
            addEmail()
            
        } else if emails.count > 1 {
            for index in (0..<emails.count).reversed() {
                Func.ifDebug(print("--- \(file).\(#function) - emails - index: \(index)"))
                if !emails[index].hasData && emails.count > 1 {
                    contact.deleteEmail(emails[index], context: context)
                    emails.remove(at: index)
                }
            }
        }
        
        if phones.count == 0 {
            addPhone()
            
        } else if phones.count > 1 {
            for index in (0..<phones.count).reversed() {
                Func.ifDebug(print("--- \(file).\(#function) - emails - index: \(index)"))
                if !phones[index].hasData && phones.count > 1 {
                    contact.deletePhone(phones[index], context: context)
                    phones.remove(at: index)
                }
            }
        }
    }
    
    func updateDisplay(doFlashScrollIndicators: Bool = false, makeFirstResponder indexPath: IndexPath? = nil) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        setNavigationForDispMode()
        setConstraintsForDispMode()
        
        if dispMode == .disp {
            nameLabel.text = contact.nameNatural
            
            sectionTypes = [DataType]()
            if phones.count > 0 {
                sectionTypes.append(.phone)
            }
            if emails.count > 0 {
                sectionTypes.append(.email)
            }
            if addresses.count > 0 {
                sectionTypes.append(.address)
            }
            if contact.dateOfBirth != nil {
                sectionTypes.append(.dateOfBirth)
            }
            
        } else {
            sectionTypes = [.name, .phone, .email, .address, .dateOfBirth]
        }
        
        tableView.reloadData()
        
        if doFlashScrollIndicators {
            tableView?.flashScrollIndicators()
        }
        
        if let indexPath = indexPath {
            setFirstResponder(indexPath: indexPath)
        }
    }
    
    func setFirstResponderForAdd() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        setFirstResponder(indexPath: IndexPath(row: 0, section: 0))
    }
    
    func setFirstResponder(indexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) === \(Func.debugDate())"))
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ContactVCCell else { return }
        
        cell.firstTextFieldBecomeFirstResponder()
    }
    
    func alertForSetUpContact(contact: Contact?, withDispMode newDispMode: ContactVCDispMode) -> UIAlertController? {
        // When newDispMode is .add
        //   - the new contact does not exist yet, so possibly create one then present it
        // When newDispMode is .disp
        //   - the contact exists, so possibly present it
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if self.contact == nil || dispMode == .disp {
            return nil
        }
        
        if dispMode != .disp && !doneBarButton.isEnabled {
            return nil
        }
        
        let alert = UIAlertController(title: "Unsaved Data", message: nil, preferredStyle: .alert)
        
        let continueAction = UIAlertAction(title: "Continue Editing", style: .default, handler: { _ in
            Func.ifDebug(print("--- \(self.file).\(#function) - handle continue"))
        })
        alert.addAction(continueAction)
        
        let saveAction = UIAlertAction(title: "Save Changes", style: .default, handler: { _ in
            Func.ifDebug(print("--- \(self.file).\(#function) - handle save"))
            self.mdHelper.coreDataStack.saveContext()
            if newDispMode == .disp {
                self.delegate?.selectAndPresentContact(contact!)
            } else {
                self.delegate?.addAndPresentContact()
            }
        })
        alert.addAction(saveAction)
        
        let discardAction = UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
            Func.ifDebug(print("--- \(self.file).\(#function) - handle discard"))
            self.context.rollback()
            if newDispMode == .disp {
                self.delegate?.selectAndPresentContact(contact!)
            } else {
                self.delegate?.addAndPresentContact()
            }
        })
        alert.addAction(discardAction)
        
        return alert
    }
    
    // MARK: - Actions
    
    @objc func cancel() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        doTransition()
        
        // delay the actual Cancel actions to make sure that textFieldDidEndEditing trimming is completed first
        
        NotificationCenter.default.post(name: NotificationName.contactVCCellShouldResignFirstResponders, object: nil)
        
        Func.afterDelay(0.1) {
            self.handleCancel()
        }
    }
    
    @objc func done() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        doTransition()
        
        // delay the actual Done actions to make sure that textFieldDidEndEditing trimming is completed first
        
        NotificationCenter.default.post(name: NotificationName.contactVCCellShouldResignFirstResponders, object: nil)
        
        Func.afterDelay(0.1) {
            self.handleDone()
        }
    }
    
    func handleCancel() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        mdHelper.coreDataStack.context.rollback()
        
        if dispMode == .add {
            mdHelper.rollBackDetailToNoContact()
        }
        
        setUpContactData()
        
        cancelDoneCommon()
    }
    
    func handleDone() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        checkData()
        
        mdHelper.coreDataStack.saveContext()
        
        cancelDoneCommon()
    }
    
    func cancelDoneCommon() {
        
        idsWithChanges = Set<NSObject>()
        dispMode = .disp
        updateDisplay(doFlashScrollIndicators: true)
    }
    
    @objc func edit() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        dispMode = .edit
        doTransition()
        updateDisplay(doFlashScrollIndicators: true)
    }
    
    @objc func deleteContact() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        mdHelper.rollBackDetailToNoContact()
        
        contact.deleteSelf(context: context)
        mdHelper.coreDataStack.saveContext()
        
        clearContactData()
    }
    
    // MARK: - Add ContactData
    
    func addAddress() {
        let labels = AddressLabel.labels(context: context)
        let newItem = Address(label: labels[0], context: context)
        contact.addAddress(newItem)
        addresses.append(newItem)
    }
    
    func addEmail() {
        let labels = EmailLabel.labels(context: context)
        let newItem = Email(label: labels[0], context: context)
        contact.addEmail(newItem)
        emails.append(newItem)
    }
    
    func addPhone() {
        let labels = PhoneLabel.labels(context: context)
        let newItem = Phone(label: labels[0], context: context)
        contact.addPhone(newItem)
        phones.append(newItem)
    }
    
    // MARK: - Keyboard
    
    @objc func keyboardNotification(_ notification: Notification) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        guard let contentInset = keyboardHelper.contentInset(forTableView: tableView, forNotification: notification) else { return }
        
        UIView.animate(withDuration: keyboardHelper.duration, delay: 0, options: keyboardHelper.animationOptions, animations: {
            self.tableView.contentInset = contentInset
        })
    }
    
    // MARK: - Change ContactData Sort
    
    @objc func longPressGestureRecognized(_ gestureRecognizer: UIGestureRecognizer) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if dispMode == .disp {
            return
        }
        
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        
        let locationInView = longPress.location(in: self.tableView)
        
        guard let indexPath = tableView.indexPathForRow(at: locationInView) else { return }
        
        struct My {
            static var cellSnapshot: UIView? = nil
            static var firstIndex = true
        }
        
        struct Path {
            static var initialIndexPath: IndexPath? = nil
            static var maxRowAllowed = 0
        }
        
        let numberOfRowsInSect = tableView.numberOfRows(inSection: indexPath.section)
        Func.ifDebug(print("=== \(file).\(#function) - indexPath: \(Func.displayPath(indexPath)), numberOfRowsInSect: \(numberOfRowsInSect)"))
        
        // Make sure only working with cells that can be sorted
        
        if My.firstIndex {
            if numberOfRowsInSect < 3 {
                // don't work with section with two or less rows (1 real entry and 1 "add")
                return
            }
            if indexPath.row == numberOfRowsInSect - 1 {
                // don't try to move the "add" cell
                return
            }
        }
        
        switch longPress.state {
        case .began:
            Func.ifDebug(print("=== \(file).\(#function) - .began === My.firstIndex: \(My.firstIndex) ==="))
            My.firstIndex = false
            Path.initialIndexPath = indexPath
            Path.maxRowAllowed = numberOfRowsInSect - 1  // can't move to the last row
            
            let cell = (self.tableView.cellForRow(at: indexPath)!)
            My.cellSnapshot = snapshotOfCell(cell)
            var center = cell.center
            My.cellSnapshot!.center = center
            My.cellSnapshot!.alpha = 0.0
            self.tableView.addSubview(My.cellSnapshot!)
            UIView.animate(withDuration: 0.2, animations: {
                center.y = locationInView.y
                My.cellSnapshot!.center = center
                My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.04, y: 1.04)
                My.cellSnapshot!.alpha = 1.0
                cell.alpha = 0.0
            }, completion: { finished in
                if finished {
                    cell.isHidden = true
                }
            })
            
        case .changed:
            Func.ifDebug(print("=== \(file).\(#function) - .changed === My.firstIndex: \(My.firstIndex) ==="))
            if Path.initialIndexPath == nil {
                return
            }
            
            if indexPath.section != Path.initialIndexPath!.section {
                // can't move a cell to another section
                return
            }
            
            if indexPath.row == Path.maxRowAllowed {
                // can't move the cell to the "add" position
                return
            }
            
            var center = My.cellSnapshot!.center
            center.y = locationInView.y
            My.cellSnapshot!.center = center
            
            if indexPath == Path.initialIndexPath! {
                // haven't moved to a different row
                return
            }
            
            moveContactData(fromIndexPath: Path.initialIndexPath!, toIndexPath: indexPath)
            
            Path.initialIndexPath = indexPath
            
        default:
            Func.ifDebug(print("=== \(file).\(#function) - default === My.firstIndex: \(My.firstIndex) ==="))
            My.firstIndex = true
            if Path.initialIndexPath == nil {
                return
            }
            let cell = tableView.cellForRow(at: Path.initialIndexPath!)!
            cell.isHidden = false
            cell.alpha = 0.0
            UIView.animate(withDuration: 0.2, animations: {
                My.cellSnapshot!.center = cell.center
                My.cellSnapshot!.transform = CGAffineTransform.identity
                My.cellSnapshot!.alpha = 0.0
                cell.alpha = 1.0
            }, completion: { finished in
                if finished {
                    Path.initialIndexPath = nil
                    My.cellSnapshot!.removeFromSuperview()
                    My.cellSnapshot = nil
                }
            })
        }
    }
    
    func snapshotOfCell(_ inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let cellSnapshot: UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    func moveContactData(fromIndexPath: IndexPath, toIndexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) - fromIndexPath: \(Func.displayPath(fromIndexPath)), toIndexPath: \(Func.displayPath(toIndexPath)) === \(Func.debugDate())"))
        
        guard let fromCell = tableView.cellForRow(at: fromIndexPath) as? ContactVCCell,
        let toCell = tableView.cellForRow(at: toIndexPath) as? ContactVCCell
        else { return }
        
        switch fromCell.dataType! {
        case .address:
            moveAddress(fromIndex: fromIndexPath.row, toIndex: toIndexPath.row)
        case .email:
            moveEmail(fromIndex: fromIndexPath.row, toIndex: toIndexPath.row)
        case .phone:
            movePhone(fromIndex: fromIndexPath.row, toIndex: toIndexPath.row)
        default:
            break
        }
        
        fromCell.checkHasChangesHasChanged()
        toCell.checkHasChangesHasChanged()
        
        tableView.moveRow(at: fromIndexPath, to: toIndexPath)
    }
    
    func moveAddress(fromIndex: Int, toIndex: Int) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let iniToSort = addresses[toIndex].sort
        addresses[toIndex].sort = addresses[fromIndex].sort
        addresses[fromIndex].sort = iniToSort
        
        addresses.swapAt(fromIndex, toIndex)
    }
    
    func moveEmail(fromIndex: Int, toIndex: Int) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let iniToSort = emails[toIndex].sort
        emails[toIndex].sort = emails[fromIndex].sort
        emails[fromIndex].sort = iniToSort
        
        emails.swapAt(fromIndex, toIndex)
    }
    
    func movePhone(fromIndex: Int, toIndex: Int) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let iniToSort = phones[toIndex].sort
        phones[toIndex].sort = phones[fromIndex].sort
        phones[fromIndex].sort = iniToSort
        
        phones.swapAt(fromIndex, toIndex)
    }
    
    // MARK: - Geocode / Map
    
    func geocode(address: String, completion: @escaping ((CLLocationCoordinate2D?) -> Void)) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        showSearchingForAddress(address)
        
        geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: { placemarks, error in
            if self.doGeocode {
                if error != nil {
                    self.geocodeSearchingAlert.dismiss(animated: false, completion: nil)
                    Func.ifDebug(print("Error: \(String(describing: error))"))
                    completion(nil)
                }
                if let placemark = placemarks?.first {
                    self.geocodeSearchingAlert.dismiss(animated: true, completion: nil)
                    let coordinates = placemark.location!.coordinate
                    //Func.ifDebug(print("--- \(self.file).\(#function) - latitude: \(coordinates.latitude), longitude: \(coordinates.longitude)"))
                    completion(coordinates)
                }
            }
        })
    }
    
    func showSearchingForAddress(_ address: String) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        let message = "Searching for the coordinates of \"\(address)\"."
        geocodeSearchingAlert = UIAlertController(title: "Preparing To Map",
                                                  message: message,
                                                  preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            self.doGeocode = false
            self.geocoder.cancelGeocode()
        })
        geocodeSearchingAlert.addAction(cancelAction)
        
        present(geocodeSearchingAlert, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension ContactVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let numberOfSects = sectionTypes.count
        
        Func.ifDebug(print("=== \(file).\(#function) - numberOfSects: \(numberOfSects)"))
        
        return numberOfSects
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        
        switch sectionTypes[section] {
        case .phone:
            numberOfRows = phones.count
            if dispMode != .disp {
                numberOfRows += 1
            }
            
        case .email:
            numberOfRows = emails.count
            if dispMode != .disp {
                numberOfRows += 1
            }
            
        case .address:
            numberOfRows = addresses.count
            if dispMode != .disp {
                numberOfRows += 1
            }
            
        default:
            numberOfRows = 1
        }
        
        Func.ifDebug(print("=== \(file).\(#function) === section: \(section) -> \(numberOfRows)"))
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        // create "add" cell
        
        if dispMode != .disp {
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                switch sectionTypes[indexPath.section] {
                case .phone, .email, .address:
                    let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.add, for: indexPath) as! ContactVCAddCell
                    cell.configure(dataType: sectionTypes[indexPath.section], delegate: self)
                    return cell
                default:
                    break
                }
            }
        }
        
        // create regular cell
        
        switch sectionTypes[indexPath.section] {
        case .name:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.name, for: indexPath) as! ContactVCNameCell
            cell.configure(contact: contact, dispMode: dispMode, delegate: self)
            return cell
            
        case .phone:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.phone, for: indexPath) as! ContactVCPhoneCell
            cell.configure(phone: phones[indexPath.row], dispMode: dispMode, delegate: self)
            return cell
            
        case .email:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.email, for: indexPath) as! ContactVCEmailCell
            cell.configure(email: emails[indexPath.row], dispMode: dispMode, delegate: self)
            return cell
            
        case .address:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.addr, for: indexPath) as! ContactVCAddrCell
            cell.configure(addr: addresses[indexPath.row], dispMode: dispMode, delegate: self)
            return cell
            
        case .dateOfBirth:
            let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.dob, for: indexPath) as! ContactVCDoBCell
            cell.configure(contact: contact, dispMode: dispMode, delegate: self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        if dispMode == .disp {
            return false
        }
        
        // disallow editing ContactVCNameCell and ContactVCDoBCell
        if indexPath == IndexPath(row: 0, section: 0) || indexPath == IndexPath(row: 0, section: sectionTypes.count - 1) {
            return false
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ContactVCCell else { return false }
        
        // confirm that the cell is a data cell
        
        if let _ = cell as? ContactVCAddCell {
            return false
        } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ContactVCCell else { return nil }
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            
            let dataType = cell.dataType!
            Func.ifDebug(print("--- \(self.file).\(#function) - delete - dataType: \(dataType)"))
            
            let checkHasData = self.dispMode == .edit
            var insertIDsWithChanges = false
            
            Func.ifDebug(print("--- \(self.file).\(#function) - delete - ........... checkHasData: \(checkHasData)"))
            Func.ifDebug(print("--- \(self.file).\(#function) - delete - ini insertIDsWithChanges: \(insertIDsWithChanges)"))
            
            switch dataType {
            case .address:
                self.addresses.remove(at: indexPath.row)
                let contactData = (cell as! ContactVCAddrCell).addr!
                if checkHasData {
                    insertIDsWithChanges = contactData.hasData
                }
                self.contact.deleteAddress(contactData, context: self.context)
            case .email:
                self.emails.remove(at: indexPath.row)
                let contactData = (cell as! ContactVCEmailCell).email!
                if checkHasData {
                    insertIDsWithChanges = contactData.hasData
                }
                self.contact.deleteEmail(contactData, context: self.context)
            case .phone:
                self.phones.remove(at: indexPath.row)
                let contactData = (cell as! ContactVCPhoneCell).phone!
                if checkHasData {
                    insertIDsWithChanges = contactData.hasData
                }
                self.contact.deletePhone(contactData, context: self.context)
            default:
                break
            }
            
            Func.ifDebug(print("--- \(self.file).\(#function) - delete - fnl insertIDsWithChanges: \(insertIDsWithChanges)"))
            
            if insertIDsWithChanges {
                self.idsWithChanges.insert("delete" as NSString)
            }
            
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.endUpdates()
        }

        return [delete]
    }
}

// MARK: - UITableViewDelegate

extension ContactVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        Func.ifDebug(print("=== \(file).\(#function) === section: \(section)"))
        
        if section == 0 {
            return nil
        }
        
        return ContactVCSectionHeaderView(dispMode: dispMode, separatorInset: tableView.separatorInset.left)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        Func.ifDebug(print("=== \(file).\(#function) === section: \(section)"))
        
        if section == 0 {
            return 0
        } else {
            // adjust the separatorInset if the view.frame.size has changed
            if view.frame.size != sizeLayedOut {
                if let view = tableView.headerView(forSection: section) as? ContactVCSectionHeaderView {
                    view.setSeparatorInset(tableView.separatorInset.left)
                }
            }
            return 0.5
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)) ==="))
        
        // handle "add" cell
        
        if dispMode != .disp {
            if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                switch sectionTypes[indexPath.section] {
                case .phone, .email, .address:
                    return indexPath
                default:
                    return nil
                }
            }
        }
        
        // handle regular cell
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.displayPath(indexPath)))"))
        
        // if the cell is a ContactVCAddCell, create a new data entry
        
        guard let addCell = tableView.cellForRow(at: indexPath) as? ContactVCAddCell else { return }
        
        idsWithChanges.insert("add" as NSString)
        
        switch addCell.dataType! {
        case .address:
            addAddress()
        case .email:
            addEmail()
        case .phone:
            addPhone()
        default:
            return
        }
        
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
        
        guard let newCell = tableView.cellForRow(at: indexPath) as? ContactVCCell else { return }
        
        newCell.firstTextFieldBecomeFirstResponder()
        
        // adjust scroll so that addCell and newCall are visible, with a preference for newCell
        
        let cellsTop = tableView.convert(newCell.frame, to: view).minY
        let cellsBot = tableView.convert(addCell.frame, to: view).maxY
        
        let visTop = tableView.frame.minY
        let visBot = tableView.frame.maxY - tableView.contentInset.bottom
        
        if cellsTop < visTop || cellsBot > visBot {
            // either the top or bottom of the cells range is not visible
            
            if cellsBot - cellsTop <= visBot - visTop {
                // the whole cells range can be visible, so scroll addCell to bottom
                let indexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            } else {
                // the whole cells range can not be visible, so scroll newCell to top
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
}

// MARK: - ContactVCCellDelegate

extension ContactVC: ContactVCCellDelegate {
    
    func hasChangesHasChanged(to hasChanges: Bool, forID id: NSObject) {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        
        if hasChanges {
            idsWithChanges.insert(id)
        } else {
            idsWithChanges.remove(id)
        }
    }
    
    func setNextFirstResponder(dataType: DataType, contactData: ContactData?) {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        
        var section: Int!
        var row = 0
        
        switch dataType {
        case .name:
            section = 1
        case .phone:
            let phone = contactData as! Phone
            let index = phones.firstIndex(where: {$0 == phone})!
            if index < phones.count - 1 {
                section = 1
                row = index + 1
            } else {
                section = 2
            }
        case .email:
            let email = contactData as! Email
            let index = emails.firstIndex(where: {$0 == email})!
            if index < emails.count - 1 {
                section = 2
                row = index + 1
            } else {
                section = 3
            }
        case .address:
            let addr = contactData as! Address
            let index = addresses.firstIndex(where: {$0 == addr})!
            if index < addresses.count - 1 {
                section = 3
                row = index + 1
            } else {
                section = 4
            }
        default:
            break
        }
        
        if let section = section {
            setFirstResponder(indexPath: IndexPath(row: row, section: section))
        }
    }
    
    func returnContext() -> NSManagedObjectContext {
        return context
    }
    
    func presentDataLabelPickerVC(_ vc: DataLabelPickerVC) {
        Func.ifDebug(print("=== \(file).\(#function) ==="))
        
        vc.mdHelper = mdHelper
        mdHelper.presentViewController(vc)
    }
    
    func placeCallTo(_ phone: Phone) {
        let number = phone.number
        Func.ifDebug(print("=== \(file).\(#function) === number: '\(number)' === \(Func.debugDate())"))
        
        guard number.isPhoneNumber else {
            let alert = UIAlertController(title: "Invalid Number", message: "This is not a valid phone number: \(number).", preferredStyle: .alert)
            
            let editAction = UIAlertAction(title: "Edit It", style: .default, handler: { _ in
                self.dispMode = .edit
                let row = self.phones.firstIndex(where: {$0 == phone})!
                let indexPath = IndexPath(row: row, section: DataType.phone.contactVCSection)
                self.updateDisplay(doFlashScrollIndicators: true, makeFirstResponder: indexPath)
            })
            alert.addAction(editAction)
            
            let okAction = UIAlertAction(title: "OK", style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            return
        }
        
        if let url = URL(string: "tel://\(phone.numberToCall)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
            
        } else {
            let alert = UIAlertController(title: "Cannot Call", message: "Your device cannot place the call to \(number).", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func sendEmailTo(_ email: Email) {
        let name = email.contact!.nameNatural
        let addr = email.addr
        
        Func.ifDebug(print("=== \(file).\(#function) === name: '\(name)', addr: '\(addr)' === \(Func.debugDate())"))
        
        guard addr.isEmailAddress else {
            let alert = UIAlertController(title: "Invalid Email", message: "This is not a valid email address: \(addr).", preferredStyle: .alert)
            
            let editAction = UIAlertAction(title: "Edit It", style: .default, handler: { _ in
                self.dispMode = .edit
                let row = self.emails.firstIndex(where: {$0 == email})!
                let indexPath = IndexPath(row: row, section: DataType.email.contactVCSection)
                self.updateDisplay(doFlashScrollIndicators: true, makeFirstResponder: indexPath)
            })
            alert.addAction(editAction)
            
            let okAction = UIAlertAction(title: "OK", style: .default)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            return
        }
        
        guard MFMailComposeViewController.canSendMail() else {
            let alert = UIAlertController(title: "Cannot Send", message: "Your device cannot send an email to \(name) at \(addr).", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            return
        }
        
        let recipients = ["\(name) <\(addr)>"]
        
        let mcVC = MailComposeHelper.makeComposer(delegate: self)
        mcVC.setToRecipients(recipients)
        
        present(mcVC, animated: true)
    }
    
    func presentMapFor(_ address: Address) {
        let name = address.contact!.nameNatural
        let addressString = address.singleLineString
        
        Func.ifDebug(print("=== \(file).\(#function) === name: '\(name)', addr: '\(addressString)' === \(Func.debugDate())"))
        
        guard NetworkHelper.isConnectedToNetwork else {
            let alert = UIAlertController(title: "No Network Connection", message: "You must be connected to a network to map this location.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            return
        }
        
        geocode(address: addressString) { coordinates in
            Func.ifDebug(print("=== \(self.file).\(#function) === got coords=== \(Func.debugDate())"))
            
            guard let coordinates = coordinates else {
                let alert = UIAlertController(title: "Address Not Found", message: "The location of the address: \(addressString) could not be found.", preferredStyle: .alert)
                
                let editAction = UIAlertAction(title: "Edit It", style: .default, handler: { _ in
                    self.dispMode = .edit
                    let row = self.addresses.firstIndex(where: {$0 == address})!
                    let indexPath = IndexPath(row: row, section: DataType.address.contactVCSection)
                    self.updateDisplay(doFlashScrollIndicators: true, makeFirstResponder: indexPath)
                })
                alert.addAction(editAction)
                
                let okAction = UIAlertAction(title: "OK", style: .default)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: coordinates, addressDictionary: address.addressDictionary))
            destination.name = name
            
            MKMapItem.openMaps(with: [destination], launchOptions: nil)
        }
    }
}

// MARK: - ContactsVCDelegate

extension ContactVC: ContactsVCDelegate {
    
    func alertForAddContact() -> UIAlertController? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        return alertForSetUpContact(contact: nil, withDispMode: .add)
    }
    
    func alertForWillSelectContact(_ contact: Contact) -> UIAlertController? {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        return alertForSetUpContact(contact: contact, withDispMode: .disp)
    }
}

// MARK: - MFMailComposeViewControllerDelegate

extension ContactVC: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if error != nil {
            Func.ifDebug(print("Error: \(String(describing: error))"))
        }
        
        controller.dismiss(animated: true, completion: nil)
        
        if let alert = MailComposeHelper.alert(forResult: result) {
            present(alert, animated: true, completion: nil)
        }
    }
}
