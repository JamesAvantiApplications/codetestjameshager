//
//  UIViewController+addRemove.swift
//
//  Created by James Hager on 8/24/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  Extension of UIViewController to simplify adding and removing child view controllers
//

import UIKit

extension UIViewController {
    
    func add(_ child: UIViewController) {
        add(child, to: view)
    }
    
    func add(_ child: UIViewController, to view: UIView) {
        addChild(child)
        child.view.add(toView: view)
        child.didMove(toParent: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
}
