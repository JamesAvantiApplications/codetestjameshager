//
//  AAContainerVC.swift
//
//  Created by James Hager on 12/20/16.
//  Copyright © 2016 James Hager. All rights reserved.
//
//  An overall container view controller to restrict master/detail presentation to iPads.
//  It wraps an AASplitVC.
//

import UIKit
import CoreData

class AAContainerVC: UIViewController {
    
    var mdHelper: MasterDetailHelper!
    
    var aaSplitVC: AASplitVC!
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - VC Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Func.ifDebug(print("========================================="))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("========================================="))
        
        // put the split view controller in the container view controller
        
        aaSplitVC = AASplitVC()
        aaSplitVC.mdHelper = mdHelper
        
        let masterVC = ContactsVC()
        masterVC.mdHelper = mdHelper
        masterVC.doFirstFetch()
        
        let masterNavController = UINavigationController(rootViewController: masterVC)
        masterNavController.title = "Show Sidebar"
        
        let detailVC = BlankVC()
        detailVC.navigationItem.leftItemsSupplementBackButton = true
        detailVC.navigationItem.leftBarButtonItem = aaSplitVC.displayModeButtonItem
        
        let detailNavController = UINavigationController(rootViewController: detailVC)
        // the following two lines set up the full-screen/collapse botton on the detail nav controller
        //detailVC.navigationItem.leftItemsSupplementBackButton = true
        //detailVC.navigationItem.leftBarButtonItem = aaSplitVC.displayModeButtonItem
        
        aaSplitVC.viewControllers = [masterNavController, detailNavController]
        aaSplitVC.forSingleViewControllerShowPrimaryOnLaunch = true
        
        mdHelper.navigationController = masterNavController
        mdHelper.detailNavController = detailNavController
        
        self.add(aaSplitVC)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Func.ifDebug(print("-----------------------------------------"))
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        Func.ifDebug(print("-----------------------------------------"))
        
        // If the device is a mixed size-class device (iPhone plus), then tell the aaSplitVC that's it's compact all around
        
        if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .compact {
            setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact), forChild: aaSplitVC)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        Func.ifDebug(print("=== \(file).\(#function) === size: (W,H)=\(size) === \(Func.debugDate())"))
        
        // If the device is a mixed size-class device (iPhone plus), then tell the aaSplitVC that's it's compact all around
        // Note: the traitCollection is for the current orientation, not the "will transition to" orientation
        
        if traitCollection.verticalSizeClass == .regular && traitCollection.horizontalSizeClass == .compact {
            setOverrideTraitCollection(UITraitCollection(horizontalSizeClass: .compact), forChild: aaSplitVC)
        }
        
        super.viewWillTransition(to: size, with: coordinator)
    }
}
