//
//  KeyboardHelper.swift
//
//  Created by James Hager on 12/8/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  A helper class to handle keyboard notifications
//

import UIKit

class KeyboardHelper {
    
    weak var view: UIView!
    
    var minHeight: CGFloat = 0
    
    var height: CGFloat!
    
    var dontReturnHeightWhenAnimating = true
    
    var doAnimation = false
    
    var keyboardIsAnimating = false
    
    var duration: TimeInterval!
    var animationOptions: UIView.AnimationOptions!
    
    lazy var file: String = Func.getSourceFileNameFromFullPath(#file)
    
    init(view: UIView) {
        self.view = view
        self.height = minHeight
    }
    
    init(view: UIView, minHeight: CGFloat) {
        self.view = view
        self.minHeight = minHeight
        self.height = minHeight
    }
    
    func height(forNotification notification: Notification) -> CGFloat? {
        //Func.ifDebug(print("=== \(file).\(#function) === \(debugDate())"))
        
        if dontReturnHeightWhenAnimating && keyboardIsAnimating {
            return nil
        }
        
        guard let userInfo = notification.userInfo,
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            else { return nil }
        
        duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions().rawValue
        animationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if endFrame.origin.y >= UIScreen.main.bounds.size.height {
            height = minHeight
            //Func.ifDebug(print("--- \(file).\(#function) - hide, height: \(height!)"))
        } else {
            height = endFrame.size.height - view.safeAreaInsets.bottom
            //Func.ifDebug(print("--- \(file).\(#function) - show, height: \(height!)"))
        }
        
        return height
    }
    
    func contentInset(forTableView tableView: UITableView, forNotification notification: Notification) -> UIEdgeInsets? {
        
        guard height(forNotification: notification) != nil else { return nil }
        
        var contentInset = tableView.contentInset
        contentInset.bottom = height
        
        return contentInset
    }
    
    func viewDidAppear() {
        doAnimation = true
    }
    
    func isAnimating() {
        keyboardIsAnimating = true
    }
    
    func stoppedAnimating() {
        keyboardIsAnimating = false
    }
}
