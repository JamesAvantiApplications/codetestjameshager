//
//  SettingsValueViewCell.swift
//
//  Created by James Hager on 3/15/16.
//  Copyright © 2016 James Hager. All rights reserved.
//

import UIKit

class SettingsValueViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIImageView(image: UIImage.imageOfColor(Color.lightLightGreen, size: CGSize(width: 1, height: 1)))
    }
    
    func configureCell(text: String, showChecked: Bool) {
        
        label.text = text
        
        tintColor = Color.mediumGreen
        
        if showChecked {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
    }
}
