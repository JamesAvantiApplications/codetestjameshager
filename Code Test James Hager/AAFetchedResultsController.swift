//
//  AAFetchedResultsController.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/29/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  override func sectionIndexTitle(forSectionName sectionName: String) -> String?
//    to just return sectionName instead of the default "capitalized first letter of the section name"
//

import CoreData

class AAFetchedResultsController: NSFetchedResultsController<NSFetchRequestResult> {

    @objc override func sectionIndexTitle(forSectionName sectionName: String) -> String? {
        return sectionName
    }
}
