//
//  MasterDetailHelper.swift
//
//  Created by James Hager on 1/2/17.
//  Copyright © 2017 James Hager. All rights reserved.
//
//  A helper class that mainly supports the master/detail operation.
//
//  If !isMasterDetail, then view controllers are presented as usual.
//
//  If isMasterDetail, then
//     - the base detail view is always a BlankVC
//     - when the SettingsVC is presented (on the master), a new BlankVC is also presented on the detail
//         - if a settings detail VC is presented, it's always one VC above the BlankVC
//     - when the SettingsVC is removed, the detail VC stack is returned to the state before the SettingsVC was presented
//

import UIKit

class MasterDetailHelper {
    
    var isMasterDetail = true
    
    let coreDataStack = CoreDataStack()
    
    var navigationController: UINavigationController!
    var detailNavController: UINavigationController!
    var blankVC: BlankVC!
    var contactVC: ContactVC!
    var contactsVC: ContactsVC!
    
    var contactPresented: Contact!
    
    var detailTopIndexBeforeBlank: Int!
    
    lazy var file = Func.getSourceFileNameFromFullPath(#file)
    
    // MARK: - View Controller Methods
    
    func presentContact(contact: Contact, dispMode: ContactVCDispMode? = nil) {
        Func.ifDebug(print("=== \(file).\(#function) === isMasterDetail: \(isMasterDetail) === \(Func.debugDate())"))
        
        if contactVC == nil {
            contactVC = ContactVC()
            contactVC.mdHelper = self
            
            if isMasterDetail && contactsVC == nil {
                if let contactsVC = navigationController!.viewControllers[0] as? ContactsVC {
                    self.contactsVC = contactsVC
                    contactsVC.delegate = contactVC
                    contactVC.delegate = contactsVC
                }
            }
        }
        
        contactVC.setUpContact(contact, dispMode: dispMode)
        
        if isMasterDetail {
            
            contactsVC?.searchBar.resignFirstResponder()
            
            if let contactPresented = contactPresented {
                if contactPresented != contact {
                    prepDetForCleanContact()
                }
            }
            
            let detailTopIndex = detailNavController.viewControllers.count - 1
            //Func.ifDebug(print("--- \(file).\(#function) - detailTopIndex: \(detailTopIndex)"))
            
            if detailTopIndex == 0 {
                detailNavController.pushViewControllerWithFade(contactVC)
            }
            
            contactPresented = contact
            
        } else {
            navigationController!.pushViewController(contactVC, animated: true)
        }
    }
    
    func presentViewController(_ vc: UIViewController) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            detailNavController.pushViewController(vc, animated: true)
            
        } else {
            navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    func removeViewController() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            detailNavController.popViewController(animated: true)

        } else {
            navigationController!.popViewController(animated: true)
        }
    }
    
    func presentViewControllerOnDetail(_ vc: UIViewController) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            let detailTopIndex = detailNavController.viewControllers.count - 1
            
            if detailTopIndex == detailTopIndexBeforeBlank + 1 {
                // present the new vc on top of the blank
                detailNavController.pushViewController(vc, animated: false)
            } else {
                // replace the vc on top of the blank
                detailNavController.replaceViewControllerWithFade(vc, atIndex: detailTopIndex)
            }
            
        } else {
            navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    func presentViewControllerOnMasterAndBlankOnDetail(vc: UIViewController) {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            detailTopIndexBeforeBlank = detailNavController.viewControllers.count - 1
            //Func.ifDebug(print("--- \(file).\(#function) - detailTopIndexBeforeBlank: \(detailTopIndexBeforeBlank!)"))
            detailNavController.pushViewController(BlankVC(), animated: detailTopIndexBeforeBlank > 0)
        }
        
        navigationController!.pushViewController(vc, animated: true)
    }
    
    func popSettingsDetail() {
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            detailNavController.popViewControllerWithFade()
            detailNavController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func rollBackDetail() {
        // roll back the detailNav stack to the reference (detailTopIndexBeforeBlank) level
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if !isMasterDetail {
            return
        }
        
        let detailTopIndex = detailNavController.viewControllers.count - 1
        let numToPop = detailTopIndex - detailTopIndexBeforeBlank
        
        /*
        Func.ifDebug(print("--- \(file).\(#function) - detailTopIndexBeforeBlank: \(detailTopIndexBeforeBlank!)"))
        Func.ifDebug(print("--- \(file).\(#function) - .......... detailTopIndex: \(detailTopIndex)"))
        Func.ifDebug(print("--- \(file).\(#function) - ................ numToPop: \(numToPop)"))
        */
        
        guard numToPop > 0 else { return }
        
        let animateLast = detailTopIndex > 1
        
        for index in 1...numToPop {
            Func.ifDebug(print("--- \(file).\(#function) - pop index \(index)"))
            detailNavController.popViewController(animated: animateLast && index == numToPop)
        }
    }
    
    func rollBackDetailToNoContact() {
        // roll back the detailNav stack to the base BlankVC
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        if isMasterDetail {
            contactPresented = nil
            
            let detailTopIndex = detailNavController.viewControllers.count - 1
            //Func.ifDebug(print("--- \(file).\(#function) - detailTopIndex: \(detailTopIndex)"))
            
            if detailTopIndex > 0 {
                detailNavController.fadeTransition()
            }
            
            for _ in 0...detailTopIndex {
                detailNavController.popViewController(animated: false)
            }
            
        } else {
            navigationController!.popViewController(animated: true)
        }
    }
    
    func prepDetForCleanContact() {
        // prepare detailNav stack to show a ContactVC
        Func.ifDebug(print("=== \(file).\(#function) === \(Func.debugDate())"))
        
        // if the detailNavController is not showing a ContactVC, pop so it is
        if let _ = detailNavController?.topViewController as? ContactVC  {
        } else {
            detailNavController.popViewController(animated: false)
        }
    }
}
