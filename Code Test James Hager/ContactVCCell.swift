//
//  ContactVCCell.swift
//  Code Test James Hager
//
//  Created by James Hager on 12/18/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//
//  The base class for ContactVC UITableViewCells
//

import UIKit
import CoreData

protocol ContactVCCellDelegate {
    func hasChangesHasChanged(to hasChanges: Bool, forID id: NSObject)
    func setNextFirstResponder(dataType: DataType, contactData: ContactData?)
    func returnContext() -> NSManagedObjectContext
    func presentDataLabelPickerVC(_ vc: DataLabelPickerVC)
    func placeCallTo(_ phone: Phone)
    func sendEmailTo(_ email: Email)
    func presentMapFor(_ address: Address)
}

// MARK: -

class ContactVCCell: UITableViewCell {
    
    var dataType: DataType!
    
    var delegate: ContactVCCellDelegate?
    
    var typeLabelChangesWithDispMode = true
    
    // override
    var hasChanges: Bool {
        return false
    }
    
    // override
    var idForChange: NSObject {
        return "" as NSString
    }
    
    var previousHasChanges = false
    
    var dataLabelPickerIndexOfSelectedValue: Int!
    
    var constraintsForDisp = [NSLayoutConstraint]()
    var constraintsForAddEdit = [NSLayoutConstraint]()
    
    var constraintsVaryWithDispMode = false
    
    // MARK: - Configure
    
    // on subclasses, call this directly
    func configure(delegate: ContactVCCellDelegate, typeLabelChangesWithDispMode: Bool? = nil) {
        self.delegate = delegate
        if let typeLabelChangesWithDispMode = typeLabelChangesWithDispMode {
            self.typeLabelChangesWithDispMode = typeLabelChangesWithDispMode
        }
        previousHasChanges = false
        selectionStyle = .none
    }
    
    // must override to handle the dispMode
    // must call super
    func setView(dispMode: ContactVCDispMode) {
        
        if !constraintsVaryWithDispMode {
            return
        }
        
        NSLayoutConstraint.deactivate(constraintsForDisp)
        NSLayoutConstraint.deactivate(constraintsForAddEdit)
        
        setViewsIsHidden(dispMode: dispMode)
        
        if dispMode == .disp {
            NSLayoutConstraint.activate(constraintsForDisp)
        } else {
            NSLayoutConstraint.activate(constraintsForAddEdit)
        }
    }
    
    // must override to handle the dispMode setup - no need to call super
    func setViewsIsHidden(dispMode: ContactVCDispMode) {
    }
    
    func setTextField(_ textField: UITextField, forDispMode dispMode: ContactVCDispMode) {
        setTextField(textField, hasAction: false, forDispMode: dispMode)
    }
    
    func setTextField(_ textField: UITextField, hasAction: Bool, forDispMode dispMode: ContactVCDispMode) {
        
        if dispMode == .disp {
            if hasAction {
                textField.textColor = Color.avantiGreen
            } else {
                textField.textColor = Color.darkGray
            }
        } else {
            textField.textColor = .black
        }
        
        if dispMode == .disp {
            textField.tag = 0  // used as a flag to not allow editing
            textField.clearButtonMode = .never
            textField.backgroundColor = .white
            textField.leftViewMode = .never
            textField.layer.borderColor = UIColor.white.cgColor
        } else {
            textField.tag = 1  // used as a flag to allow editing
            textField.clearButtonMode = .always
            textField.backgroundColor = Color.lightGray
            textField.leftViewMode = .always
            textField.layer.borderColor = Color.lightGray.cgColor
        }
    }
    
    func setTextField(_ textField: UITextField, forEditing: Bool, animate: Bool) {
        
        var backgroundColor: UIColor!
        var borderColor: CGColor!
        
        if forEditing {
            backgroundColor = Color.lightLightGreen
            borderColor = Color.lightishGreen.cgColor
        } else {
            backgroundColor = Color.lightGray
            borderColor = Color.lightGray.cgColor
        }
        
        if animate {
            let caAnimation = CABasicAnimation(keyPath: "borderColor")
            caAnimation.fromValue = textField.layer.borderColor
            caAnimation.toValue = borderColor
            caAnimation.duration = 0.15
            textField.layer.add(caAnimation, forKey: "borderColor")
            textField.layer.borderColor = borderColor
            
            UIView.animate(withDuration: 0.15) {
                textField.backgroundColor = backgroundColor
            }
            
        } else {
            textField.backgroundColor = backgroundColor
            textField.layer.borderColor = borderColor
        }
    }
    
    // MARK: - Setup View
    
    // override to do the view setup
    // must call super at the end
    func setUpView() {
        setConstraintsVaryWithDispMode()
    }
    
    func makeLabel(textStyle: UIFont.TextStyle? = nil) -> UILabel {
        let label = UILabel(frame: CGRect.zero)
        if let textStyle = textStyle {
            label.font = UIFont.preferredFont(forTextStyle: textStyle)
        }
        label.adjustsFontForContentSizeCategory = true
        return label
    }
    
    func makeTextField(placeholder: String? = nil) -> UITextField {
        let textField = UITextField(frame: CGRect.zero)
        setUpTextField(textField, placeholder: placeholder)
        return textField
    }
    
    func setUpTextField(_ textField: UITextField, placeholder: String? = nil) {
        textField.font = UIFont.preferredFont(forTextStyle: .body)
        textField.adjustsFontForContentSizeCategory = true
        textField.autocorrectionType = .no
        textField.delegate = self
        textField.returnKeyType = .next
        textField.enablesReturnKeyAutomatically = false
        textField.addTarget(self, action: #selector(textFieldEdited(_:)), for: .editingChanged)
        
        if let placeholder = placeholder {
            textField.placeholder = placeholder
        }
        
        let leftView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 6, height: 8)))
        textField.leftView = leftView
        textField.leftViewMode = .never
        
        textField.layer.borderWidth = 1
        textField.layer.borderColor = UIColor.white.cgColor
        textField.layer.cornerRadius = 6
    }
    
    func setUpTypeLabel(_ typeLabel: UILabel, andDisclosureView disclosureView: UIView) {
        typeLabel.add(toView: contentView, pinTypes: [.layoutMargin, nil, nil, .layoutMargin])
        
        disclosureView.add(toView: contentView, pinTypes: [.layoutMargin, .layoutMargin, nil, nil])
        disclosureView.widthAnchor.constraint(equalToConstant: 2).isActive = true
        disclosureView.heightAnchor.constraint(equalTo: typeLabel.heightAnchor).isActive = true
        
        let typeLabelRightDisp = typeLabel.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor)
        let typeLabelRightAddEdit = typeLabel.rightAnchor.constraint(equalTo: disclosureView.leftAnchor)
        
        constraintsForDisp = [typeLabelRightDisp]
        constraintsForAddEdit = [typeLabelRightAddEdit]
        
        let disclosure = UITableViewCell()
        disclosure.accessoryType = .disclosureIndicator
        disclosure.add(toView: disclosureView, pinTypes: [nil, .superview, nil, nil])
        disclosure.widthAnchor.constraint(equalToConstant: 2).isActive = true
        disclosure.centerYAnchor.constraint(equalTo: disclosureView.centerYAnchor).isActive = true
    }
    
    func setConstraintsVaryWithDispMode() {
        constraintsVaryWithDispMode = (constraintsForDisp.count > 0) || (constraintsForAddEdit.count > 0)
    }
    
    // MARK: - Actions
    
    // override to make the first textField the first responder
    func firstTextFieldBecomeFirstResponder() {
    }
    
    func showDataLabelPickerVC(values: [String], delegate: DataLabelPickerVCDelegate) {
        let vc = DataLabelPickerVC()
        vc.setUp(viewTitle: dataType.rawValue.capitalized + " Label", values: values, indexOfSelectedValue: dataLabelPickerIndexOfSelectedValue, delegate: delegate)

        self.delegate?.presentDataLabelPickerVC(vc)
    }
    
    func checkHasChangesHasChanged() {
        let hasChanges = self.hasChanges
        
        Func.ifDebug(print("=== ContactVCCell.\(#function) === hasChanges/previousHasChanges: \(hasChanges)/\(previousHasChanges)"))
        
        if hasChanges == previousHasChanges {
            return
        }
        
        previousHasChanges = hasChanges
        
        delegate?.hasChangesHasChanged(to: hasChanges, forID: idForChange)
    }
    
    @objc func textFieldEdited(_ textField: UITextField) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        handleTextFieldEdited(textField)
    }
    
    // override
    func handleTextFieldEdited(_ textField: UITextField) {
    }
}

// MARK: - UIGestureRecognizerDelegate

extension ContactVCCell {
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        guard let textField = gestureRecognizer.view as? UITextField else { return true }
        
        Func.ifDebug(print("--- \(#function) - tag: \(tag) (return true for 0)"))
        
        return textField.tag == 0
    }
}

// MARK: - UITextFieldDelegate

extension ContactVCCell: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        Func.ifDebug(print("=== \(#function) ==="))
        
        return textField.tag == 1
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Func.ifDebug(print("=== \(#function) ==="))
        
        setTextField(textField, forEditing: true, animate: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // don't allow leading whitespaces
        Func.ifDebug(print("=== \(#function) === range: \(range), replacementString: '\(string)'"))
        
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let trimmedText = newText.trimmedLeadingWhitespaces()
        
        if newText == trimmedText {
            return true
        }
        
        textField.text = trimmedText
        
        handleTextFieldEdited(textField)
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // trim (leading and) trailing whitespaces
        Func.ifDebug(print("=== \(#function) ==="))
        
        setTextField(textField, forEditing: false, animate: true)
        
        let textOnEntry = textField.text!
        let textTrimmed = (textField.text! as NSString).trimmingCharacters(in: .whitespaces)
        
        if textTrimmed != textOnEntry {
            textField.text = textTrimmed
            handleTextFieldEdited(textField)
        }
    }
}
