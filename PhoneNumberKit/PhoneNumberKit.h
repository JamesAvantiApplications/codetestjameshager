//
//  PhoneNumberKit.h
//  PhoneNumberKit
//
//  Created by James Hager on 12/28/18.
//  Copyright © 2018 Avanti Applications, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PhoneNumberKit.
FOUNDATION_EXPORT double PhoneNumberKitVersionNumber;

//! Project version string for PhoneNumberKit.
FOUNDATION_EXPORT const unsigned char PhoneNumberKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PhoneNumberKit/PublicHeader.h>


